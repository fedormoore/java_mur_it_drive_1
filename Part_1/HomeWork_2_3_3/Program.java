import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double a = scanner.nextDouble();
		int b = scanner.nextInt();

		int[] ns = {100, 1000, 10000, 100000, 1000000};
        int[] ys = {2, 3, 4, 5, 6, 7};

        for (int i = 0; i < ys.length - 1; i++) {
            for (int j = 0; j < ns.length - 1; j++) {
                System.out.println("Simpson - " + ns[j] + " - x^" + ys[i] + "=" + integralSimpson(a, b, ns[j], ys[i]));
            }
        }

        for (int i = 0; i < ys.length - 1; i++) {
            for (int j = 0; j < ns.length - 1; j++) {
                System.out.println("Rectangles - " + ns[j] + " - x^" + ys[i] + "=" + integralRectangles(a, b, ns[j], ys[i]));
            }
        }


	}

	public static double pow (double x, int y){
		double result = x;
		for(int i = 1; i<=y-1; i++){
			result *= x;
		}
		return result;
	}

	public static double getH(double a, double b, int n){
		return (b - a) / n;
	}

	public static double integralRectangles(double a, double b, int n, int y){
		double h = getH(a, b, n);

		double result = 0;

		for(double x = a; x <= b; x = x + h){
			double currentRectangle = pow(x, y) * h;
			result = result + currentRectangle;
		}

		return result;
	}

	public static double integralSimpson(double a, double b, int n, int y){
		double h = getH(a, b, n);

		double result = 0;

		for(double x = a; x <= b; x = x + 2 * h){
			double currentSimpson = pow(x - h, y) + 4 * pow(x, y) + pow(x + h, y);
			result = result + currentSimpson;
		}

		return result * (h / 3);
	}
}