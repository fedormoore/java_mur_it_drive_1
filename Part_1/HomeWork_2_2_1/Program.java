import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		int n = scanner.nextInt();

		integralRectangles(a, b, n);
		integralSimpson(a, b, n);
	}

	public static void integralRectangles(double a, double b, int n){
		double h = getH(a, b, n);

		double result = 0;

		int inter = 1;

		for(double x = a; x <= b; x = x + h){
			double currentRectangle = f(x) * h;
			result = result + currentRectangle;
			System.out.println ("Rectangle "+ inter +" = " + result);
			inter++;
		}
	}

	public static void integralSimpson(double a, double b, int n){
		double h = getH(a, b, n);

		double result = 0;

		int inter = 1;

		for(double x = a; x <= b; x = x + 2 * h){
			double currentSimpson = f(x - h) + 4 * f(x) + f(x + h);
			result = result + currentSimpson;
			System.out.println ("Simpson "+ inter +" = " + result * (h / 3));
			inter++;
		}
	}

	public static double getH(double a, double b, int n){
		return (b - a) / n;
	}

	public static double f(double x){
		return x * x;
	}

}