import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//указываем размер массива
		int arrayLength = scanner.nextInt();
		//создаем массив
		int array[] = new int[arrayLength];
		//присваиваем значение массиву
		for(int i = 0; i < arrayLength; i++){
			array[i] = scanner.nextInt();
		}	
		//вызываем процедуру сортировки массива
		sorting(array);
	}

	public static void sorting (int arrey[]){
		
		for(int i = 0; i < arrey.length; i++){
			for(int j = i + 1; j < arrey.length; j++){
				if (arrey[i] > arrey[j]){
					int min = arrey[i];
					arrey[i] = arrey[j];
					arrey[j] = min;
				}
			}
		}

		for(int i = 0; i < arrey.length; i++){
			System.out.print (" " + arrey[i]);
		}
	}
}