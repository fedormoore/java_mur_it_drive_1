public class RemoteController {

    private Tv tv;

    void on(int channelNumber) {
        tv.setChannelNumber(channelNumber);
    }

    void setTv(Tv tv) {
        this.tv = tv;
    }
}
