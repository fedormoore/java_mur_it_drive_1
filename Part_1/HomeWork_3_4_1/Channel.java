﻿import java.util.Random;

public class Channel {

    private Program program[];

    private char nameChannel[];

    private char nameBroadcast[];

    Channel() {
        program = new Program[10];
        program[0] = new Program();
        program[0].setBroadcast("Сделано из вторсырья".toCharArray());
        program[1] = new Program();
        program[1].setBroadcast("Все о животных".toCharArray());
        program[2] = new Program();
        program[2].setBroadcast("Расследование авиакатастроф".toCharArray());
        program[3] = new Program();
        program[3].setBroadcast("Как это устроено?".toCharArray());
        program[4] = new Program();
        program[4].setBroadcast("Как это сделано?".toCharArray());
        program[5] = new Program();
        program[5].setBroadcast("Всё о кошках".toCharArray());
        program[6] = new Program();
        program[6].setBroadcast("Всё о собаках".toCharArray());
        program[7] = new Program();
        program[7].setBroadcast("Сто к одному".toCharArray());
        program[8] = new Program();
        program[8].setBroadcast("Пятеро на одного".toCharArray());
        program[9] = new Program();
        program[9].setBroadcast("Чудо техники".toCharArray());
    }

    void setNameChannel(char[] nameChannel) {
        this.nameChannel = new char[nameChannel.length];
        this.nameChannel=nameChannel;
    }

    char[] getNameChannel() {
        showProgram();
        return this.nameChannel;
    }

    void showProgram(){
        Random random = new Random();
        this.nameBroadcast = program[random.nextInt(10)].getBroadcast();
        System.out.println(this.nameBroadcast);
    }


}
