import java.util.Random;

public class Code {

    public static void main(String[] args) {
        Channel channel[] = new Channel[5];
        channel[0] = new Channel();
        channel[0].setNameChannel("Animal Planet".toCharArray());
        channel[1] = new Channel();
        channel[1].setNameChannel("Discovery channel".toCharArray());
        channel[2] = new Channel();
        channel[2].setNameChannel("Discovery science".toCharArray());
        channel[3] = new Channel();
        channel[3].setNameChannel("H2.0".toCharArray());
        channel[4] = new Channel();
        channel[4].setNameChannel("Nat geo channel".toCharArray());

        Random random = new Random();

        Tv tv = new Tv();
        RemoteController remoteController = new RemoteController();

        remoteController.setTv(tv);
        tv.setRemoteController(remoteController);
        tv.setChannel(channel);

        remoteController.on(random.nextInt(5));
    }
}
