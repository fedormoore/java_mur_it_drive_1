public class Program {

    private char broadcast[];

    void setBroadcast(char[] broadcast) {
        this.broadcast = new char[broadcast.length];
        this.broadcast=broadcast;
    }

    char[] getBroadcast() {
        return this.broadcast;
    }
}
