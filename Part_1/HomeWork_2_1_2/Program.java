import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int dig = scanner.nextInt();

		System.out.println(mirrorDig(dig));
	}

	public static int mirrorDig(int dig){
		int mirrorDig = 0;
		while(dig > 0){
			mirrorDig *= 10;
			mirrorDig += dig%10;
			dig /= 10;
		}
		return mirrorDig;
	}

}