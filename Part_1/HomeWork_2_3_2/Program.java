import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		//указываем размер массива
		int arrayLength = scanner.nextInt();
		//создаем массив
		int array[] = new int[arrayLength];
		//присваиваем значение массиву
		for(int i = 0; i < arrayLength; i++){
			array[i] = scanner.nextInt();
		}	
		//вызываем процедуру сортировки массива
		sorting(array);

		int arraySearch = scanner.nextInt();
		//вызываем функцию бинарного поиска
		boolean result =  search(array, arraySearch);
		if (result == true){
			System.out.println("Exists");
		}else{
			System.out.println("Not exists");
		}
	}

	public static void sorting (int arrey[]){
		
		for(int i = 0; i < arrey.length; i++){
			for(int j = i + 1; j < arrey.length; j++){
				if (arrey[i] > arrey[j]){
					int min = arrey[i];
					arrey[i] = arrey[j];
					arrey[j] = min;
				}
			}
		}
	}

	public static boolean search (int arrey[], int arraySearch){
		boolean result = false;

		int l = 0;
		int r = arrey.length - 1;
		int m = arrey.length;

		while(l <= r){
			m = (l + r) / 2;
			if (arrey[m] < arraySearch){
				l = m + 1;
			} else if (arrey[m] > arraySearch){
				r = m - 1;
			} else {
				result = true;
				break;
			}
		}

		return result;
	}
}