import java.util.Scanner;

public class Program{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char a[] = scanner.nextLine().toCharArray();
        System.out.println(toInt(a));
    }

    public static boolean isDigit(char a) {
        return a >= '0' && a <= '9';
    }

    public static int toInt(char a[]) {
        int result = 0;
        for (int i = 0; i < a.length; i++) {
            if (isDigit(a[i])) {
                result *= 10;
                result += a[i] - 48;
            }
        }
        return result;
    }
}