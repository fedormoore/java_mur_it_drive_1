package ru.moore;

public class Main {
    public static void main(String[] args) {

        User user1 = User.builder()
                .firstName("Фёдор")
                .age(34)
                .build();


        User user2 = User.builder()
                .age(24)
                .lastName("Мур")
                .build();
    }
}
