package ru.moore;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String address;

    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.address = builder.address;
    }

    public static Builder builder() {
        return new Builder();
    }

    static class Builder {
        String firstName;
        String lastName;
        int age;
        String address;

        public Builder() {

        }

        public Builder firstName(String firstName) {
            this.firstName=firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName=lastName;
            return this;
        }

        public Builder age(int age) {
            this.age=age;
            return this;
        }

        public Builder address(String address) {
            this.address=address;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }
}
