class Program{

    public static void main(String[] args) {
        int result = fib(4, 1, 1);
        System.out.println("result = " + result);
    }

    public static int fib(int n, int n1, int n2) {
        if (n == 1 || n == 2) {
            return 1;
        }

        int n3 = n2 + n1;

        n1 = n2;

        n2 = n3;

        int result = fib(n - 1, n1, n2);

        if (n2 > result) {
            result = n2;
        }

        return result;
    }
}