import java.util.Scanner;

class Program{

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int dig = scanner.nextInt();
		
		System.out.println(returnSum(dig));
	}

	public static int returnSum(int dig){
		int sum = 0;
		while(dig > 0){
			sum += dig%10;
			dig /= 10;
		}
		return sum;
	}
}