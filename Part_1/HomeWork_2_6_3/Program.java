import java.util.Scanner;

public class Program{

    public static void main(String[] args) {
        char text[] = {'b', 'b', 'a', 'a', 'c', 'c'};

        System.out.print(search(text));
    }

    public static char[] search(char[] text) {
        int sumChar[] = new int[65536];
        for (int i = 0; i < text.length; i++) {
            sumChar[text[i]]++;
        }

        char abc[] = new char[6];

        for (int i = 0; i < sumChar.length; i++) {
            if (sumChar[i] > 0) {
                if (sumChar[i] > abc[1] - 48) {
                    abc[5] = abc[3];
                    abc[4] = abc[2];

                    abc[3] = abc[1];
                    abc[2] = abc[0];

                    abc[1] = (char) (sumChar[i] + 48);
                    abc[0] = (char) (i);
                } else if (sumChar[i] > abc[3] - 48) {
                    abc[3] = (char) (sumChar[i] + 48);
                    abc[2] = (char) (i);
                } else if (sumChar[i] > abc[5] - 48) {
                    abc[5] = (char) (sumChar[i] + 48);
                    abc[4] = (char) (i);
                }
            }
        }
        return abc;
    }
}