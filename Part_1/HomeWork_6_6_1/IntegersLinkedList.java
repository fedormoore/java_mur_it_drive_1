public class IntegersLinkedList {
    private Node first;
    private Node last;
    private int count;
    private int tempIndex;

    public IntegersLinkedList() {
        this.count = 0;
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.setNext(newNode);
            this.last = newNode;
        }
        this.count++;
    }

    public void add(int element, int index) {
        if (index <= count) {
            Node temp = this.first;
            Node newNode = new Node(element);
            int coun = 1;
            while (temp != null) {
                if (coun == index) {
                    newNode.setNext(temp.getNext());
                    temp.setNext(newNode);
                }
                this.last = temp;
                temp = temp.getNext();
                coun++;
            }
            this.count++;
        }
    }

    public int get(int index) {
        if (index <= count) {
            Node temp = this.first;
            int coun = 1;
            while (temp != null) {
                if (coun == index) {
                    return temp.getValue();
                }
                temp = temp.getNext();
                coun++;
            }
        }
        return -1;
    }

    public void remove(int index) {
        if (index <= count) {
            Node temp = this.first;
            int coun = 1;
            while (temp != null) {
                if (count == index && coun == count - 1) {
                    temp.setNext(null);
                    this.last = temp;
                    break;
                }
                if (coun == index) {
                    Node temp1 = temp.getNext();
                    temp.setValue(temp1.getValue());
                    temp.setNext(temp1.getNext());
                    break;
                }
                temp = temp.getNext();
                coun++;
            }
            count--;
        }
    }

    public void reverse() {
        Node temp = this.first;
        for (int i = 0; i < count / 2; i++) {
            
            int tempValue = temp.getValue();

            temp.setValue(get(count - i));

            Node temp2 = this.first;
            for (int j = 1; j < count - i; j++) {
                temp2 = temp2.getNext();
            }
            temp2.setValue(tempValue);

            temp = temp.getNext();
        }
    }
}
