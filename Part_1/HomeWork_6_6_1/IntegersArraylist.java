public class IntegersArraylist {

    private static final int DEFAULT_ARRAY_SIZE = 10;
    private int elements[];
    private int count;

    public IntegersArraylist() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void outOfIndex() {
        int tempElements[] = new int[(int) (this.elements.length * 1.5)];
        for (int i = 0; i < this.elements.length; i++) {
            tempElements[i] = this.elements[i];
        }
        this.elements = tempElements;
    }

    public void add(int element) {
        if (count >= this.elements.length) {
            outOfIndex();
        }
        this.elements[count] = element;
        count++;
    }

    public void add(int element, int index) {
        if (count >= this.elements.length) {
            outOfIndex();
        }
        if (index <= count-1) {
            for (int i = count; i > index; i--) {
                this.elements[i] = this.elements[i - 1];
            }
            this.elements[index] = element;
        } else {
            System.err.println("index > count");
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Index out of range");
            return -1;
        }
    }

    public void remove(int index) {
        if (index <= count-1) {
            for (int i = index; i < count; i++) {
                this.elements[i] = this.elements[i + 1];
            }
            count--;
        } else {
            System.err.println("Index out of range");
        }
    }

    public void reverse() {
        for (int i = 0; i < this.elements.length / 2; i++) {
            int temp = this.elements[i];
            this.elements[i] = this.elements[this.elements.length - i - 1];
            this.elements[this.elements.length - i - 1] = temp;
        }
    }
}
