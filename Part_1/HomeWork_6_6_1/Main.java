import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        IntegersArraylist list = new IntegersArraylist();

        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.add(11, 5);
        list.remove(5);
        list.reverse();

        IntegersLinkedList linkedlist = new IntegersLinkedList();
        linkedlist.add(1);
        linkedlist.add(2);
        linkedlist.add(3);
        linkedlist.add(11,3);
        linkedlist.get(2);
        linkedlist.remove(3);
        linkedlist.reverse();
    }
}
