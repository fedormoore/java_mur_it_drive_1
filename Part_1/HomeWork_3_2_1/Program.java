import java.util.Scanner;
import java.util.Random;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("������� �������� �����?");
        int n = scanner.nextInt();

        Random random = new Random();

        char nameHuman[][] = new char[10][];
        nameHuman[0] = "����".toCharArray();
        nameHuman[1] = "����".toCharArray();
        nameHuman[2] = "����".toCharArray();
        nameHuman[3] = "����".toCharArray();
        nameHuman[4] = "������".toCharArray();
        nameHuman[5] = "������".toCharArray();
        nameHuman[6] = "����".toCharArray();
        nameHuman[7] = "�����".toCharArray();
        nameHuman[8] = "������".toCharArray();
        nameHuman[9] = "����".toCharArray();

        int sumAge[][] = new int[50][2];

        Human humans[] = new Human[100];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].name = nameHuman[random.nextInt(10)];
            humans[i].age = random.nextInt(50);

            sumAge[humans[i].age][0] = humans[i].age;
            sumAge[humans[i].age][1]++;
        }

        for (int i = 0; i < sumAge.length; i++) {
            for (int j = i + 1; j < sumAge.length; j++) {
                if (sumAge[i][1] < sumAge[j][1]) {
                    int temp[] = sumAge[i];
                    sumAge[i] = sumAge[j];
                    sumAge[j] = temp;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println("������� " + sumAge[i][0] + " � " + sumAge[i][1] + " �������");
        }
    }
}