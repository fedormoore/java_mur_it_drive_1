class Program {

    public static void main(String[] args) {
        char a[] = {'a', 'a', 'b'};
        char b[] = {'a', 'a', 'b', 'a'};

        System.out.println(stringCompare(a, b));
    }

    public static int stringCompare(char a[], char b[]) {
	int result = 0;

        int length = a.length;
        if (a.length < b.length) {
            length = b.length;
        }

        for (int i = 0; i < length; i++) {
            if (i > b.length - 1) {
                result = 1;
                break;
            }
            if (i > a.length - 1) {
                result = -1;
                break;
            }
            if (a[i] < b[i]) {
                result = -1;
                break;
            } else if (a[i] > b[i]) {
                result = 1;
                break;
            }
        }
        return result;
    }
}