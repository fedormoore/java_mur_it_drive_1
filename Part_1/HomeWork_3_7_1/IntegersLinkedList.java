package ru.moore;

public class IntegersLinkedList {
    private Node first;
    private Node last;
    private int count;

    public IntegersLinkedList() {
        this.count = 0;
    }

    private static class Node {
        private int value;
        private Node next;

        Node(int value) {
            this.value = value;
        }
    }

    class LinkedListIterator {
        Node current = first;

        LinkedListIterator() {

        }

        boolean hasNext() {
            return current != null;
        }

        int next() {
            int nextElement = current.value;
            current = current.next;
            return nextElement;
        }
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode;
            this.last = newNode;
        }
        this.count++;
    }

    public void add(int element, int index) {
        if (index <= count) {
            Node temp = this.first;
            Node newNode = new Node(element);
            int coun = 1;
            while (temp != null) {
                if (coun == index) {
                    newNode.next = temp.next;
                    temp.next = newNode;
                }
                this.last = temp;
                temp = temp.next;
                coun++;
            }
            this.count++;
        }
    }

    public int get(int index) {
        if (index <= count) {
            Node temp = this.first;
            int coun = 1;
            while (temp != null) {
                if (coun == index) {
                    return temp.value;
                }
                temp = temp.next;
                coun++;
            }
        }
        return -1;
    }

    public void remove(int index) {
        if (index <= count) {
            Node temp = this.first;
            int coun = 1;
            while (temp != null) {
                if (count == index && coun == count - 1) {
                    temp.next = null;
                    this.last = temp;
                    break;
                }
                if (coun == index) {
                    Node temp1 = temp.next;
                    temp.value = temp1.value;
                    temp.next = temp1.next;
                    break;
                }
                temp = temp.next;
                coun++;
            }
            count--;
        }
    }

    public void reverse() {
        Node current = this.first;
        for (int i = 0; i < count / 2; i++) {

            int tempValue = current.value;

            current.value = get(count - i);

            Node next = this.first;
            for (int j = 1; j < count - i; j++) {
                next = next.next;
            }
            next.value = tempValue;

            current = current.next;
        }
    }
}
