package ru.moore;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        IntegersLinkedList linkedlist = new IntegersLinkedList();
        linkedlist.add(1);
        linkedlist.add(2);
        linkedlist.add(3);
        linkedlist.add(11, 3);
        linkedlist.get(2);
        linkedlist.remove(3);
        linkedlist.reverse();

        IntegersLinkedList.LinkedListIterator iterator = linkedlist.new LinkedListIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
