package ru.moore;

import javax.sql.DataSource;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;

public class EntityManager {

    private DataSource dataSource;

    public EntityManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> void createTable(String tableName, Class<T> entityClass) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();

            Field fields[] = entityClass.getDeclaredFields();
            StringBuilder temp = new StringBuilder();
            for (Field field : fields) {
                if (field.getType().getSimpleName().equals("String")) {
                    temp.append(field.getName() + " varchar(255), ");
                } else if (field.getType().getSimpleName().equals("Long")) {
                    temp.append(field.getName() + " integer, ");
                } else {
                    temp.append(field.getName() + " " + field.getType().getSimpleName() + ", ");
                }
            }

            StringBuilder fieldTable = new StringBuilder();
            fieldTable.append(temp.substring(0, temp.length() - 2));

            String sqlQuery = "CREATE TABLE " + tableName + " (" + fieldTable + ");";
            statement = connection.prepareStatement(sqlQuery);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, null, connection);
        }
    }

    public void save(String tableName, Object entity) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();

            StringBuilder tempNameField = new StringBuilder();
            StringBuilder tempValue = new StringBuilder();
            for (Method method : entity.getClass().getDeclaredMethods()) {
                if (method.getName().startsWith("get")) {

                    tempNameField.append(method.getName().substring(3).toLowerCase() + ", ");

                    if (method.getReturnType().getSimpleName().equals("String")) {
                        tempValue.append("'" + method.invoke(entity) + "', ");
                    } else {
                        tempValue.append(method.invoke(entity) + ", ");
                    }
                }
            }

            StringBuilder nameField = new StringBuilder();
            nameField.append(tempNameField.substring(0, tempNameField.length() - 2));

            StringBuilder valueField = new StringBuilder();
            valueField.append(tempValue.substring(0, tempValue.length() - 2));

            String sqlQuery = "INSERT INTO " + tableName + " (" + nameField + ")" + " VALUES (" + valueField + ");";
            statement = connection.prepareStatement(sqlQuery);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            closedStatementResult(statement, null, connection);
        }
    }

    public <T, ID> T findById(String tableName, Class<T> resultType, Class<ID> idType, ID idValue) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;

        String SQL_FIND_USER_BY_LOGIN_AND_PASSWORD = "select * from " + tableName + " where id = ?";
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_USER_BY_LOGIN_AND_PASSWORD);
            if (idType.getSimpleName().equals("Long")) {
                statement.setLong(1, (Long) idValue);
            }

            resultSet = statement.executeQuery();
            ResultSetMetaData rsmd = resultSet.getMetaData();
            if (resultSet.next()) {
                Class<?> aClass = resultType;
                Object object1 = aClass.newInstance();

                for (Method method : aClass.getDeclaredMethods()) {
                    if (method.getName().startsWith("set")) {

                        String nameMethod = method.getName();
                        Class<?>[] typeMethod = method.getParameterTypes();

                        Method method1 = object1.getClass().getMethod(nameMethod, typeMethod);

                        int columnIndex = Integer.valueOf(resultSet.findColumn(method.getName().substring(3).toLowerCase()));
                        if (rsmd.getColumnTypeName(columnIndex).equals("varchar")) {
                            method1.invoke(object1, resultSet.getString(method.getName().substring(3).toLowerCase()));
                        }
                        if (rsmd.getColumnTypeName(columnIndex).equals("int4")) {
                            method1.invoke(object1, resultSet.getLong(method.getName().substring(3).toLowerCase()));
                        }
                        if (rsmd.getColumnTypeName(columnIndex).equals("bool")) {
                            method1.invoke(object1, resultSet.getBoolean(method.getName().substring(3).toLowerCase()));
                        }
                    }
                }
                return (T) object1;
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
        return null;
    }

    private void closedStatementResult(PreparedStatement statement, ResultSet resultSet, Connection connection) {
        try {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ignored) {
        }
    }
}
