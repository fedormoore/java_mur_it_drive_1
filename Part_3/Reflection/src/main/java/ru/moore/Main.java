package ru.moore;

import javax.sql.DataSource;
import java.lang.reflect.*;

public class Main {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/user_chat";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty555";


    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        DataSource dataSource = new DataSourceSimpleImpl(DB_URL, DB_USER, DB_PASSWORD
                , "org.postgresql.Driver");

        EntityManager entityManager = new EntityManager(dataSource);

        Class<?> aClass = Class.forName("ru.moore.User");
        entityManager.createTable("user1", aClass);

        Object object = aClass.newInstance();
        Constructor<?> constructor = object.getClass().getConstructor(Long.class, String.class, String.class, boolean.class);
        object = constructor.newInstance(1L, "test1", "test2", true);

        entityManager.save("user1", object);

        User user = entityManager.findById("user1", User.class, Long.class, 1L);
    }

}
