import com.google.auto.service.AutoService;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.SneakyThrows;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.File;
import java.io.FileWriter;
import java.util.*;


@AutoService(Processor.class)
@SupportedAnnotationTypes(value = {"HtmlForm", "HtmlInput"})
public class HtmlProcessor extends AbstractProcessor {

    @SneakyThrows
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
        configuration.setDefaultEncoding("UTF-8");
        configuration.setTemplateLoader(new FileTemplateLoader(new File("src/main/resources")));
        Template template = configuration.getTemplate("template_for_web.ftlh");

        Map<String, Object> attributes = new HashMap<>();

        attributes=getHtmlForm(roundEnv, attributes);

        List<Map<String, String>> htmlInputs = getHtmlInputs(roundEnv);

        attributes.put("htmlInputs", htmlInputs);

        FileWriter fileWriter = new FileWriter("output.html");
        template.process(attributes, fileWriter);
        return true;
    }

    private Map<String, Object> getHtmlForm(RoundEnvironment roundEnv, Map<String, Object> attributes){
        Set<? extends Element> formElements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);
        for (Element formElement : formElements) {
            HtmlForm annotationHtmlForm = formElement.getAnnotation(HtmlForm.class);
            attributes.put("action", annotationHtmlForm.action());
            attributes.put("method", annotationHtmlForm.method());
        }
        return attributes;
    }

    private List<Map<String, String>> getHtmlInputs(RoundEnvironment roundEnv){
        Set<? extends Element> inputElements = roundEnv.getElementsAnnotatedWith(HtmlInput.class);
        List<Map<String, String>> htmlInputs = new ArrayList<>();
        for (Element inputElement : inputElements) {
            Map<String, String> tempHtmlInput = new HashMap<>();
            HtmlInput annotationHtmlInput = inputElement.getAnnotation(HtmlInput.class);
            tempHtmlInput.put("type", annotationHtmlInput.type());
            tempHtmlInput.put("name", annotationHtmlInput.name());
            tempHtmlInput.put("placeholder", annotationHtmlInput.placeholder());
            htmlInputs.add(tempHtmlInput);
        }
        return htmlInputs;
    }
}
