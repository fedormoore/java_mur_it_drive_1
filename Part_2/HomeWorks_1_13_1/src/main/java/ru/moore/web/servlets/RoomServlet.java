package ru.moore.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.moore.web.service.ChatService;
import ru.moore.web.config.ApplicationConfig;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/chooseRoom")
public class RoomServlet extends HttpServlet{

private ChatService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(ChatService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameRoom = req.getParameter("nameRoom");
        String maxUsers = req.getParameter("maxUsers");
        service.createRoom(nameRoom, Integer.valueOf(maxUsers));
    }
}
