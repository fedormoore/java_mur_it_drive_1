package ru.moore.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.moore.web.config.ApplicationConfig;
import ru.moore.web.models.FilesUser;
import ru.moore.web.service.ChatService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/filesList")
public class FilesListServlet extends HttpServlet {

    private ChatService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(ChatService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuidUser = null;
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("UUID")) {
                uuidUser = cookie.getValue();
            }
        }
        List<FilesUser> files = service.getAllFiles(uuidUser);
        if (files.size()>0) {
            PrintWriter writer = resp.getWriter();

            String page = "<!DOCTYPE html>\n" +
                    "<html lang=\"ru\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Title</title>\n" +
                    "</head>\n" +
                    "<body>\n";
            for (FilesUser file : files) {
                page += "<a href=http://localhost:8080/files?id="+file.getFileName() +">http://localhost:8080/files?id="+file.getFileName() +"</a><br>";//"<h1>" + file.getFileName() + "</h1>\n";
            }
            page += "</body>\n" +
                    "</html>";
            writer.println(page);
        }
    }
}
