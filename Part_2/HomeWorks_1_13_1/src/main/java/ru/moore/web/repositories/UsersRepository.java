package ru.moore.web.repositories;

import ru.moore.web.models.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User> {

    Optional<User> findByLogin(String login, String password);

}
