package ru.moore.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import ru.moore.web.models.FilesUser;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.repositories.FilesUserRepository;
import ru.moore.web.repositories.RoomRepository;
import ru.moore.web.repositories.UsersRepository;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ChatServiceImpl implements ChatService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private FilesUserRepository fileRepository;
    @Autowired
    private Environment environment;

    @Override
    public User signUp(String login, String password) {
        Optional<User> user = usersRepository.findByLogin(login, password);
        if (!user.isPresent()) {
            User userNew = User.builder()
                    .login(login)
                    .password(password)
                    .uuid(UUID.randomUUID().toString())
                    .build();
            usersRepository.save(userNew);
            return userNew;
        }
        return user.get();
    }

    @Override
    public Optional<User> signIn(String login, String password) {
        Optional<User> user = usersRepository.findByLogin(login, password);
        if (user.isPresent()) {
            user.get().setUuid(UUID.randomUUID().toString());
            usersRepository.update(user.get());
        }
        return user;
    }

    @Override
    public void createRoom(String roomName, Integer maxUsers) {
        Room room = Room.builder()
                .nameRoom(roomName)
                .maxUsers(maxUsers)
                .build();
        Optional.ofNullable(roomRepository.findByNameRoom(roomName)).orElse(roomRepository.save(room));
    }

    @Override
    public void fileUpload(String fileName, String uuidUser, String fileType) {
        FilesUser file = FilesUser.builder()
                .fileName(fileName)
                .uuidUser(uuidUser)
                .fileType(fileType)
                .build();
        fileRepository.save(file);
    }

    @Override
    public List<FilesUser> getAllFiles(String uuidUser) {
        return fileRepository.findByFile(uuidUser);
    }

    @Override
    public Optional<FilesUser> getFile(String uuidFile) {
        return fileRepository.find(uuidFile);
    }

    @Override
    public String generateUuid() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void saveFile(InputStream inputStream, String fileType, String uuidUser) {
        try {

            String fileName = generateUuid();

            Files.copy(inputStream, Paths.get(environment.getProperty("patchToFileUsers") + fileName));

            fileUpload(fileName, uuidUser, fileType);

            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
