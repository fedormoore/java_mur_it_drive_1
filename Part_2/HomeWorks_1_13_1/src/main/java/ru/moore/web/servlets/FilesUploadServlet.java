package ru.moore.web.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.moore.web.config.ApplicationConfig;
import ru.moore.web.service.ChatService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private ChatService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(ChatService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("file");
        String fileType = part.getContentType();
        InputStream inputStream = part.getInputStream();

        String uuidUser = null;
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("UUID")){
                uuidUser = cookie.getValue();
            }
        }

        service.saveFile(inputStream, fileType, uuidUser);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("html/userChat.html");
        requestDispatcher.forward(req, resp);
    }
}

