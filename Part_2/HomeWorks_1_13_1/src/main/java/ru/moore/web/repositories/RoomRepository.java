package ru.moore.web.repositories;

import ru.moore.web.models.Room;
import ru.moore.web.models.User;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room> {

    Room findByNameRoom(String roomName);

}
