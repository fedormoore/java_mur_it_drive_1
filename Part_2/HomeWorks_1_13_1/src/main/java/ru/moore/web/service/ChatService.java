package ru.moore.web.service;

import ru.moore.web.models.FilesUser;
import ru.moore.web.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public interface ChatService {

    User signUp(String login, String password);

    Optional<User> signIn(String login, String password);

    void createRoom(String roomName, Integer maxUsers);

    void fileUpload(String fileName, String uuidUser, String fileType);

    List<FilesUser> getAllFiles(String uuidUser);

    Optional<FilesUser> getFile(String uuidFile);

    String generateUuid();

    void saveFile(InputStream inputStream, String fileType, String uuidUser);
}
