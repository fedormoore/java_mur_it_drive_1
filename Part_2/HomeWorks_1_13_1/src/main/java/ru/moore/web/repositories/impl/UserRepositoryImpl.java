package ru.moore.web.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.repositories.RowMapper;
import ru.moore.web.repositories.UsersRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryImpl implements UsersRepository {

    private DataSource dataSource;

    private final String SQL_INSERT_USER = "insert into users (login, password, uuid) values (?, ?, ?)";
    private final String SQL_UPDATE_USER = "update users set login = ?, password = ?, uuid = ? where id = ?";
    private final String SQL_SELECT_USER = "select * from users where login = ? and password = ?";

    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            List<Room> rooms = new ArrayList<>();
            return new User(
                    row.getLong("id"),
                    row.getString("login"),
                    row.getString("password"),
                    row.getString("uuid"),
                    rooms,
                    new Room()
            );
        }
    };

    @Override
    public Optional<User> findByLogin(String login, String password) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER);
            statement.setString(1, login);
            statement.setString(2, password);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.of(userRowMapper.mapRow(resultSet));
            } else {
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
    }

    @Override
    public User save(User object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getLogin());
            statement.setString(2, object.getPassword());
            statement.setString(3, object.getUuid());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("User save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys, connection);
        }
        return object;
    }

    @Override
    public void update(User object) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_USER);
            statement.setString(1, object.getLogin());
            statement.setString(2, object.getPassword());
            statement.setString(3, object.getUuid());
            statement.setLong(4, object.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, null, connection);
        }
    }

    @Override
    public Optional<User> find(String login) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    private void closedStatementResult(PreparedStatement statement, ResultSet resultSet, Connection connection) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ignored) {
        }
    }
}
