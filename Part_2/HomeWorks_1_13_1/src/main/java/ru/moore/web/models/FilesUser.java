package ru.moore.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilesUser {

    private Long id;
    private String fileName;
    private String uuidUser;
    private String fileType;
}
