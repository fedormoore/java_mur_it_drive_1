package ru.moore.web.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import ru.moore.web.config.ApplicationConfig;
import ru.moore.web.models.FilesUser;
import ru.moore.web.service.ChatService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

@WebServlet("/files")
public class FilesViewerServlet extends HttpServlet {

    @Autowired
    private Environment environment;

    private ChatService service;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        service = context.getBean(ChatService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameFile = req.getParameter("id");
        Optional<FilesUser> filesUser = service.getFile(nameFile);
        if (filesUser.isPresent()) {
            File file = new File(environment.getProperty("patchToFileUsers") + nameFile);
            resp.setContentType(filesUser.get().getFileType());
            resp.setContentLength((int) file.length());
            resp.setHeader("Content-Disposition", "filename=\"" + nameFile + "\"");
            Files.copy(file.toPath(), resp.getOutputStream());
            resp.flushBuffer();
        }
    }
}
