package ru.moore.repositories;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.util.List;

public interface  MessageRepositories extends CrudRepository<Message>{

    List<Message> findAllMessage(Room room);

    void insert(User user, Room room, String message);
}
