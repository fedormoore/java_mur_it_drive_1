package ru.moore.repositories;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoomRepositoriesImpl implements RoomRepositories {

    private Connection connection;

    private static final String SQL_GET_ROOM = "select r.id as id, r.name as name from room as r where name = ?";
    private static final String SQL_SET_ROOM = "insert into room (name) values (?)";
    private static final String SQL_SET_USERROOM = "insert into userroom (id_user, id_room) values (?, ?)";
    private static final String SQL_DELETE_ROOM = "delete from usersroom where id_users = ? and id_room = ?";

    public RoomRepositoriesImpl(Connection connection) {
        this.connection = connection;
    }

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            List<User> users = new ArrayList<User>();
            return new Room(
                    row.getInt("id"),
                    row.getString("name"),
                    users
            );
        }
    };

    @Override
    public Room findRoom(String nameRoom, int idUser) {
        Room room = null;
        try {
            ResultSet resultSet = findRoomSys(nameRoom);
            if (resultSet.next()) {
                room = roomRowMapper.mapRow(resultSet);
            } else {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_ROOM);
                preparedStatement.setString(1, nameRoom);
                preparedStatement.executeUpdate();
                preparedStatement.close();

                resultSet = findRoomSys(nameRoom);
                resultSet.next();
                room = roomRowMapper.mapRow(resultSet);
            }

            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_USERROOM);
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, room.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

        return room;
    }

    @Override
    public void deleteRoom(User user, Room room) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ROOM);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, room.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

    }

    public ResultSet findRoomSys(String nameRoom) {
        ResultSet resultSet;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_ROOM);
            preparedStatement.setString(1, nameRoom);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        return resultSet;
    }

    @Override
    public void save(Room object) {

    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Integer id) {

    }
}
