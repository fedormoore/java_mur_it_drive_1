package ru.moore.repositories;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoriesImpl implements UserRepositories {

    private Connection connection;

    List<Room> rooms = new ArrayList<Room>();

    private static final String SQL_GET_USER = "select u.*, r.*, ra.* from users as u\n" +
            "inner join userroom as ur on ur.id_user = u.id\n" +
            "inner join room as r on r.id = ur.id_room\n" +
            "inner join room as ra on ra.id = u.active_room\n" +
            "where login = ?";

    public UserRepositoriesImpl(Connection connection) {
        this.connection = connection;
    }

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            return new User(
                    row.getInt("id"),
                    row.getString("login"),
                    rooms,
                    new Room()
            );
        }
    };

    public User findUserByLogin(String nameUser) {
        User user = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USER);
            preparedStatement.setString(1, nameUser);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = userRowMapper.mapRow(resultSet);
                user.setActiveRoom(new Room(resultSet.getInt(6), resultSet.getString(7)));
                for (int i=0; i<resultSet.getRow(); i++){
                    rooms.add(new Room (resultSet.getInt(4), resultSet.getString(5)));
                    resultSet.next();
                }
            }
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        return user;
    }

    @Override
    public void save(User object) {

    }

    @Override
    public void update(User object) {

    }

    @Override
    public void delete(Integer id) {

    }
}
