package ru.moore;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.*;

import java.util.List;

public class ChatService {

    private MessageRepositories messageRepositories;
    private RoomRepositories roomRepositories;
    private UserRepositories userRepositories;

    public ChatService(MessageRepositories messageRepositories, RoomRepositories roomRepositories, UserRepositories userRepositories) {
        this.messageRepositories = messageRepositories;
        this.roomRepositories = roomRepositories;
        this.userRepositories = userRepositories;
    }

    public User findUserByLogin(String nameUser){
        return userRepositories.findUserByLogin(nameUser);
    }

    public Room selectRoom(String room, int idUser){
        return roomRepositories.findRoom(room, idUser);
    }

    public void deleteRoom(User user, Room room){
        roomRepositories.deleteRoom(user, room);
    }

    public void insertMessage(User user, Room room, String message){
        messageRepositories.insert(user, room, message);
    }

    public List<Message> findAllMessage(Room room){
        return messageRepositories.findAllMessage(room);
    }
    /*
    public int getIdUsers(String nameUser){
        return userRepositories.findUserByLogin(nameUser);
    }

    public int getIdRoom(String nameRoom){
        return roomRepositories.getIdRoom(nameRoom);
    }

    public int getUserRoom(int idUser, int idRoom){
        return userRoomRepositories.getUserRoom(idUser, idRoom);
    }
*/
}
