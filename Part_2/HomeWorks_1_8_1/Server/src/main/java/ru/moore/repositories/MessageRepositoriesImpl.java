package ru.moore.repositories;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoriesImpl implements MessageRepositories {

    private Connection connection;

    private static final String SQL_GET_MESSAGE = "select * from message where id_room = ? order by id desc limit 30";
    private static final String SQL_INSERT_MESSAGE = "insert into message (id_users, id_room, message) values (?, ?, ?)";

    public MessageRepositoriesImpl(Connection connection) {
        this.connection = connection;
    }

    private RowMapper<Message> messageRowMapper = new RowMapper<Message>() {
        public Message mapRow(ResultSet row) throws SQLException {
            return new Message(
                    row.getInt("id"),
                    new User(),
                    new Room(),
                    row.getString("message")
            );
        }
    };

    private RowMapper<Message> courseRowMapper = new RowMapper<Message>() {
        public Message mapRow(ResultSet row) throws SQLException {
            return new Message(
                    row.getInt("id"),
                    new User(),
                    new Room(),
                    row.getString("message")
            );
        }
    };

    public void insert(User user, Room room, String messageText) {
        Message message = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MESSAGE, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, room.getId());
            preparedStatement.setString(3, messageText);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            message = messageRowMapper.mapRow(resultSet);
            message.setUser(user);
            message.setRoom(room);
            message.setMessage(messageText);
            preparedStatement.close();
            System.out.println(message);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Message> findAllMessage(Room room) {
        try {
            List<Message> result = new ArrayList<Message>();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_MESSAGE);
            preparedStatement.setInt(1, room.getId());
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Message course = courseRowMapper.mapRow(resultSet);
                result.add(course);
            }

            preparedStatement.close();

            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public void save(Message object) {

    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Integer id) {

    }
}
