package ru.moore;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters (separators = "=")
public class Arguments {

    @Parameter(names = {"-port"})
    public String port;

}