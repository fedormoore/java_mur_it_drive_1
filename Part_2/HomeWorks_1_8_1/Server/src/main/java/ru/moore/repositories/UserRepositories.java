package ru.moore.repositories;

import ru.moore.models.Room;
import ru.moore.models.User;

public interface  UserRepositories extends CrudRepository<User>{
    User findUserByLogin(String nameUser);
}
