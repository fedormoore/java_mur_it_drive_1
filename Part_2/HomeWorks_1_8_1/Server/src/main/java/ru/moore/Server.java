package ru.moore;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Server {

    private Socket socketClient;

    private LinkedList<ThreadClient> userList = new LinkedList<ThreadClient>();

    public void start(int port, ChatService chatService) throws Exception {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                socketClient = serverSocket.accept();
                ThreadClient threadClient = new ThreadClient(socketClient, chatService);
                threadClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class ThreadClient extends Thread {

        private Socket socketClient;
        private PrintWriter writer;
        private BufferedReader reader;
        private String inputLine;

        private User user;
        private Room room;
        private Message message;

        ChatService chatService;


        public ThreadClient(Socket socketClient, ChatService chatService) {
            this.chatService = chatService;

            this.socketClient = socketClient;
            try {
                synchronized (userList) {
                    userList.add(this);
                }
                InputStream clientInputStream = socketClient.getInputStream();
                reader = new BufferedReader(new InputStreamReader(clientInputStream));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                writer = new PrintWriter(socketClient.getOutputStream(), true);
                writer.println("Enter login");

                user = chatService.findUserByLogin(reader.readLine());
                if (user.getActiveRoom().getId() > 0) {
                    List<Message> courseList = chatService.findAllMessage(user.getActiveRoom());
                    for (int i = 0; i < courseList.size(); i++)
                        writer.println(courseList.get(i).getMessage());
                }


                while (true) {
                    inputLine = reader.readLine();
                    while (inputLine != null) {
                        synchronized (userList) {
                            if (inputLine.indexOf("choose room") == 0) {
                                room = chatService.selectRoom(inputLine.substring(12, inputLine.length()), user.getId());
                            }
                            if (inputLine.indexOf("send message") == 0) {
                                chatService.insertMessage(user, user.getActiveRoom(), inputLine.substring(13, inputLine.length()));
                            }
                            if (inputLine.indexOf("exit from room") == 0) {
                                for (int i = 0; i < user.getRooms().size(); i++) {
                                    if (user.getRooms().get(i).getNameRoom().indexOf(inputLine.substring(15, inputLine.length()))==0){
                                        chatService.deleteRoom(user, user.getRooms().get(i));
                                    }
                                }
                            }
                            System.out.println(user);
/*
                            if (inputLine.indexOf("send message") == 0) {
                                if (chatService.getUserRoom(idUser, idRoom) != 0) {

                                    chatService.insert(idUser, idRoom, inputLine.substring(13, inputLine.length()));

                                    for (ThreadClient co : userList) {
                                        if (co.idRoom == idRoom && co.idUser != idUser) {
                                            writer = new PrintWriter(co.socketClient.getOutputStream(), true);
                                            writer.println(nameUser + " -> " + inputLine.substring(13, inputLine.length()));
                                        }
                                    }
                                } else {
                                    writer = new PrintWriter(socketClient.getOutputStream(), true);
                                    writer.println("select room");
                                }
                            }*/

                        }
                        inputLine = reader.readLine();
                    }
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
