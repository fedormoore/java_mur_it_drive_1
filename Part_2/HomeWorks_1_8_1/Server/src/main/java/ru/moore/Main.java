package ru.moore;

import com.beust.jcommander.JCommander;
import ru.moore.repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/user_chat";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty555";

    public static void main(String[] args) throws Exception {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        MessageRepositories messageRepositories = new MessageRepositoriesImpl(connection);
        RoomRepositories roomRepositories = new RoomRepositoriesImpl(connection);
        UserRepositories userRepositories = new UserRepositoriesImpl(connection);

        ChatService service = new ChatServiceImpl(messageRepositories,roomRepositories, userRepositories);

        Server server = new Server();
        //server.start(Integer.valueOf(arguments.port));
        server.start(7575, service);
    }

}
