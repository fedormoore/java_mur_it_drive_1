package ru.moore.repositories;

import ru.moore.models.Room;
import ru.moore.models.User;

public interface  RoomRepositories extends CrudRepository<Room>{
    Room findRoom(String nameRoom, int idUser);
    void deleteRoom(User user, Room room);
}
