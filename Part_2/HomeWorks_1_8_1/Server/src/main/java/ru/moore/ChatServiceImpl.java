package ru.moore;

import ru.moore.ChatService;
import ru.moore.repositories.MessageRepositories;
import ru.moore.repositories.RoomRepositories;
import ru.moore.repositories.UserRepositories;

public class ChatServiceImpl extends ChatService {

    public ChatServiceImpl(MessageRepositories messageRepositories, RoomRepositories roomRepositories, UserRepositories userRepositories) {
        super(messageRepositories, roomRepositories, userRepositories);
    }
}
