package ru.moore.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Room {

    private Integer id;
    private String nameRoom;
    private List<User> users;

    public Room(Integer id, String nameRoom) {
        this.id = id;
        this.nameRoom = nameRoom;
    }
}
