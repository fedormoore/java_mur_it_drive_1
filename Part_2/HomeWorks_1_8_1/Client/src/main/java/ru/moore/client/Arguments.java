package ru.moore.client;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(separators = "=")
public class Arguments {

    @Parameter(names = "-serverParameters", listConverter = ArgumentsListConverter.class)
    public List<String> serverParameters;

}
