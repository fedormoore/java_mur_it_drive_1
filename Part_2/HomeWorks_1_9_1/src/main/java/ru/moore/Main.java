package ru.moore;

import com.beust.jcommander.JCommander;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) throws Exception {
        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        SocketServer server = context.getBean(SocketServer.class);
        server.start(7575);
    }

}
