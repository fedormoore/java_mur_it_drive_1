package ru.moore;

import ru.moore.models.Room;
import ru.moore.models.User;

public interface MessagesService {

    User signUp(String login);

    Room selectRoom(String room, User user);

    void sendMessage(User user, Room room, String message);
}
