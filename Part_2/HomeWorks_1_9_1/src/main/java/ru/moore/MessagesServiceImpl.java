package ru.moore;

import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.MessageRepository;
import ru.moore.repositories.RoomRepository;
import ru.moore.repositories.UsersRepository;

public class MessagesServiceImpl implements MessagesService {

    private UsersRepository usersRepository;
    private RoomRepository roomRepository;
    private MessageRepository messageRepository;

    public MessagesServiceImpl(UsersRepository usersRepository, RoomRepository roomRepository) {
        this.usersRepository = usersRepository;
        this.roomRepository = roomRepository;
    }

    @Override
    public User signUp(String login) {
        return usersRepository.findUserByLogin(login);
    }

    @Override
    public Room selectRoom(String room, User user) {
        usersRepository.update(user);
        return roomRepository.selectRoom(room, user);
    }

    @Override
    public void sendMessage(User user, Room room, String message) {
        messageRepository.sendMessage(user, room, message);
    }

}
