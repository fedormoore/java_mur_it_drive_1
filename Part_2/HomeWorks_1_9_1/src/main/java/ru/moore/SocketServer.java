package ru.moore;

import ru.moore.models.Room;
import ru.moore.models.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SocketServer {

    private MessagesService messagesService;

    private List<ChatSocketClient> anonyms = new ArrayList<>();
    private Map<Long, List<ChatSocketClient>> users = new HashMap<>();

    public SocketServer(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    public void start(int port) {
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);

            while (true) {
                Socket socketClient = serverSocket.accept();
                ChatSocketClient threadClient = new ChatSocketClient(socketClient);
                threadClient.start();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class ChatSocketClient extends Thread {

        private Socket socket;
        private BufferedReader input;
        private PrintWriter output;

        private String inputLine;

        private Long userId;
        private Long roomId;

        private User user;
        private Room room;

        public ChatSocketClient(Socket socket) {
            this.socket = socket;

            try {
                synchronized (anonyms) {
                    anonyms.add(this);
                }
                InputStream clientInputStream = socket.getInputStream();
                input = new BufferedReader(new InputStreamReader(clientInputStream));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    inputLine = input.readLine();
                    synchronized (users) {
                        if (inputLine.indexOf("signUp") == 0) {
                            user = messagesService.signUp(inputLine.substring(7, inputLine.length()));
                            anonyms.remove(this);
                            userId = user.getId();
                            roomId = user.getActiveRoom().getId();

                            sysUsers();
                        }
                        if (inputLine.indexOf("choose room") == 0) {
                            room = messagesService.selectRoom(inputLine.substring(12, inputLine.length()), user);
                            roomId = user.getActiveRoom().getId();

                            sysUsers();
                        }
                        if (inputLine.indexOf("exit from room") == 0) {

                        }
                        if (inputLine.indexOf("send message") == 0) {
                            for (int i = 0; i < users.get(user.getActiveRoom().getId()).size(); i++) {
                                output = new PrintWriter(users.get(user.getActiveRoom().getId()).get(i).socket.getOutputStream(), true);
                                output.println(user.getName() + " -> " + inputLine.substring(13, inputLine.length()));
                                messagesService.sendMessage(user, room, inputLine.substring(13, inputLine.length()));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }

        private void sysUsers() {
            if (roomId != 0) {
                List<ChatSocketClient> lt = users.get(roomId);
                if (lt == null) {
                    lt = new ArrayList<>();
                }
                lt.add(this);
                users.put(roomId, lt);
            }
        }
    }
}
