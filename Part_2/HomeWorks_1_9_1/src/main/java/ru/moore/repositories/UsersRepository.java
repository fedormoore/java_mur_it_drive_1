package ru.moore.repositories;

import ru.moore.models.User;

public interface UsersRepository extends CrudRepository<User> {

    User findUserByLogin(String login);

}
