package ru.moore.repositories.impl;

import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.RoomRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoomRepositoryImpl implements RoomRepository {

    private DataSource dataSource;

    private static final String SQL_GET_ROOM = "select r.id as id, r.name as name from room as r where name = ?";
    private static final String SQL_SET_ROOM = "insert into room (name) values (?)";
    private static final String SQL_SET_USERROOM = "insert into userroom (id_user, id_room) values (?, ?)";
    private static final String SQL_GET_USERROOM = "select * from userroom where id_user = ? and id_room =  ?";

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Room selectRoom(String nameRoom, User user) {
        Room room = new Room();
        List<User> users = new ArrayList<User>();
        try {
            ResultSet resultSet = getRoom(nameRoom);
            if (!resultSet.next()) {
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_ROOM);
                preparedStatement.setString(1, nameRoom);
                preparedStatement.executeUpdate();

                resultSet = getRoom(nameRoom);
                resultSet.next();
            }
            setActiveRoom(user.getId(), resultSet.getLong(1));

            room.setId(resultSet.getLong(1));
            room.setNameRoom(resultSet.getString(2));
            users.add(user);
            room.setUsers(users);
            user.setActiveRoom(room);

        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

        return room;
    }

    public ResultSet getRoom(String nameRoom) {
        ResultSet resultSet;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_ROOM);
            preparedStatement.setString(1, nameRoom);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        return resultSet;
    }

    public ResultSet setActiveRoom(Long id_user, Long id_room) {
        ResultSet resultSet;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USERROOM);
            preparedStatement.setLong(1, id_user);
            preparedStatement.setLong(2, id_room);
            resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                preparedStatement = connection.prepareStatement(SQL_SET_USERROOM);
                preparedStatement.setLong(1, id_user);
                preparedStatement.setLong(2, id_room);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
        return resultSet;
    }

    @Override
    public void save(Room object) {

    }

    @Override
    public void update(Room object) {

    }

    @Override
    public void delete(Integer id) {

    }
}
