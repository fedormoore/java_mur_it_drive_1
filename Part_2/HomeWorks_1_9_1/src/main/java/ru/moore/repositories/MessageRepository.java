package ru.moore.repositories;

import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;

public interface MessageRepository extends CrudRepository<Message> {

    void sendMessage(User user, Room room, String message);

}
