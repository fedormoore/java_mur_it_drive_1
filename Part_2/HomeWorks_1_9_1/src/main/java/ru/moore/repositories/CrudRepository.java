package ru.moore.repositories;

public interface CrudRepository<T> {

    void save(T object);

    void update(T object);

    void delete(Integer id);

}
