package ru.moore.repositories.impl;

import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.UsersRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UsersRepository {

    private DataSource dataSource;

    private final String SQL_INSERT_USER = "insert into users (login) values (?)";
    private final String SQL_UPDATE_USER = "update users set active_room  = ? where id = ?";
    private final String SQL_SELECT_USER = "select u.*, r.*, ra.* from users as u\n" +
            "left join userroom as ur on ur.id_user = u.id\n" +
            "left join room as r on r.id = ur.id_room\n" +
            "left join room as ra on ra.id = u.active_room\n" +
            "where login = ?";

    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User findUserByLogin(String login) {
        User user = new User();
        List<Room> rooms = new ArrayList<Room>();
        try {
            ResultSet resultSet = getUser(login);
            if (!resultSet.next()) {
                Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER);
                statement.setString(1, login);
                statement.executeUpdate();

                resultSet = getUser(login);
                resultSet.next();
            }
            user.setId(resultSet.getLong(1));
            user.setName(resultSet.getString(2));
            user.setActiveRoom(new Room(resultSet.getLong(6), resultSet.getString(7), null));
            for (int i = 0; i < resultSet.getRow(); i++) {
                rooms.add(new Room(resultSet.getLong(4), resultSet.getString(5), null));
                resultSet.next();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return user;
    }

    private ResultSet getUser(String login) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SELECT_USER);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public void save(User object) {

    }

    public void update(User object) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_USER);
            statement.setLong(1, object.getActiveRoom().getId());
            statement.setLong(2, object.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void delete(Integer id) {

    }

}
