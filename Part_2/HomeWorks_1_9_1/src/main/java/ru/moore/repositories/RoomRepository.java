package ru.moore.repositories;

import ru.moore.models.Room;
import ru.moore.models.User;

public interface RoomRepository extends CrudRepository<Room> {

    Room selectRoom(String nameRoom, User user);

}
