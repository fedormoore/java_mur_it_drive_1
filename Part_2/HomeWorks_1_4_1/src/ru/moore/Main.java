package ru.moore;

public class Main {

    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(5);

        for (int i = 0; i < 10; i++) {
            threadPool.submit(() -> {
                for (int j = 0; j < 5; j++) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        throw new IllegalArgumentException(e);
                    }
                    System.out.println(j);
                }
            });
        }
    }
}
