package ru.moore.lesson;

public class FileInformation {
    String nameFile;
    long sizeFile;

    public FileInformation(String nameFile, long sizeFile) {
        this.nameFile = nameFile;
        this.sizeFile = sizeFile;
    }
}
