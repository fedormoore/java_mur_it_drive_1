package ru.moore.lesson;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetFileInfo {

    public Map print(String folder) {
        File folderEntries = new File(folder);

        Map<String, List<FileInformation>> map = new HashMap<>();
        FileInformation fileInformation[] = new FileInformation[folderEntries.listFiles().length];
        for (int i = 0; i < folderEntries.listFiles().length; i++) {
            fileInformation[i] = new FileInformation(folderEntries.listFiles()[i].getName(), folderEntries.listFiles()[i].length());
        }
        map.put(folder, Arrays.asList(fileInformation));
        return map;
    }

}