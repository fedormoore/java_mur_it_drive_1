package ru.moore.lesson;

import com.beust.jcommander.JCommander;

import java.util.List;
import java.util.Map;

public class Program {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);


        ThreadsService threadsService = new ThreadsService();

        for (int i = 0; i < arguments.folder.size(); i++) {
            String folder = arguments.folder.get(i);

            threadsService.submit(() -> {
                GetFileInfo getFileInfo = new GetFileInfo();
                Map<String, List<FileInformation>> map = getFileInfo.print(folder);
                for (Map.Entry<String, List<FileInformation>> entry : map.entrySet()) {
                    System.out.println("Patch name - " + entry.getKey());
                    List<FileInformation> list = entry.getValue();
                    for (int j = 0; j < list.size(); j++) {
                        System.out.println("  File name - " + list.get(j).nameFile + ", file size - " + list.get(j).sizeFile);
                    }
                }
            });
        }
    }
}
