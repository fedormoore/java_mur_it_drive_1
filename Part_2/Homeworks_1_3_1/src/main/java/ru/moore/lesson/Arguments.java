package ru.moore.lesson;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters(separators = "=")
public class Arguments {

    @Parameter(names = "-folder", listConverter = ArgumentsListConverter.class)
    public List<String> folder;

}