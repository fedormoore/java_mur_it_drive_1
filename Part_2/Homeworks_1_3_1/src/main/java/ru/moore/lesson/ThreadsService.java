package ru.moore.lesson;

public class ThreadsService {
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
