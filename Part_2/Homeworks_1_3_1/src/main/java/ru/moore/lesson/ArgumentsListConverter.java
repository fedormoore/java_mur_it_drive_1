package ru.moore.lesson;

import com.beust.jcommander.IStringConverter;

import java.util.ArrayList;
import java.util.List;

public class ArgumentsListConverter implements IStringConverter<List<String>> {

    @Override
    public List<String> convert(String folder) {
        String[] paths = folder.split(",");
        List<String> folderList = new ArrayList<>();
        for (String path : paths) {
            folderList.add(path);
        }
        return folderList;
    }
}
