package ru.moore.web.service;

import ru.moore.web.models.User;

public interface SignService {

    User signUp(String login, String password);

}
