package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.web.models.User;
import ru.moore.web.repositories.UsersRepository;
import ru.moore.web.service.SignService;

import java.util.Optional;
import java.util.UUID;

@Component
public class SignServiceImpl implements SignService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public User signUp(String login, String password) {
        Optional<User> user = usersRepository.findByLogin(login, password);
        if (!user.isPresent()) {
            User userNew = User.builder()
                    .login(login)
                    .password(password)
                    .uuid(UUID.randomUUID().toString())
                    .build();
            usersRepository.save(userNew);
            return userNew;
        }
        return user.get();
    }
}
