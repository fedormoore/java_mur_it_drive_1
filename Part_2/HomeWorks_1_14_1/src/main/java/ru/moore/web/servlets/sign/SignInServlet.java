package ru.moore.web.servlets.sign;

import org.springframework.context.ApplicationContext;
import ru.moore.web.service.AuthenticationService;
import ru.moore.web.service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private AuthenticationService authentication;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        authentication = springContext.getBean(AuthenticationService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("html/signIn.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (authentication.authenticated(login, password)) {
            Cookie cookie = new Cookie("UUID", authentication.getAuthUser().getUuid().trim());
            resp.addCookie(cookie);
            HttpSession session = req.getSession();
            session.setAttribute("authenticated", true);
            //session.setMaxInactiveInterval(60);
            resp.sendRedirect("/userChat");
        } else {
            resp.setStatus(403);
            req.getRequestDispatcher("html/signIn.html").forward(req, resp);
        }
    }
}
