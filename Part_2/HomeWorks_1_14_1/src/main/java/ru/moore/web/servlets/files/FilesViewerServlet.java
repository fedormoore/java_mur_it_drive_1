package ru.moore.web.servlets.files;

import org.springframework.context.ApplicationContext;
import ru.moore.web.models.FilesUser;
import ru.moore.web.service.FileService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

@WebServlet("/files")
public class FilesViewerServlet extends HttpServlet {

    private FileService fileService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        fileService = springContext.getBean(FileService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nameFile = req.getParameter("id");
        Optional<FilesUser> filesUser = fileService.getFile(nameFile);
        if (filesUser.isPresent()) {
            File file = new File(fileService.getPath() + nameFile);
            resp.setContentType(filesUser.get().getFileType());
            resp.setContentLength((int) file.length());
            resp.setHeader("Content-Disposition", "filename=\"" + nameFile + "\"");
            Files.copy(file.toPath(), resp.getOutputStream());
            resp.flushBuffer();
        }
    }
}
