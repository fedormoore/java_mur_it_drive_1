package ru.moore.web.service;

import ru.moore.web.models.User;

public interface AuthenticationService {

    boolean authenticated(String login, String password);

    User getAuthUser();

    Boolean isAuth();

}
