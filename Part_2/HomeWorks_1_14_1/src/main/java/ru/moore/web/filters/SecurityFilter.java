package ru.moore.web.filters;

import org.springframework.context.ApplicationContext;
import ru.moore.web.service.SignService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {

    private SignService signService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        signService = springContext.getBean(SignService.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        if (isProtected(req.getRequestURI())) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } else {
            HttpSession session = req.getSession(false);
            if (session != null) {
                Boolean authenticated = (Boolean) session.getAttribute("authenticated");
                if (authenticated != null && authenticated) {
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
            resp.setStatus(403);
            resp.sendRedirect("/signIn");
        }
    }

    private boolean isProtected(String path) {
        if (path.startsWith("/signIn") || path.startsWith("/signUp") || path.startsWith("/favicon.ico")) {
            return true;
        }
        return false;
    }

    @Override
    public void destroy() {

    }

}
