package ru.moore.web.filters;

import org.springframework.context.ApplicationContext;
import ru.moore.web.service.AuthenticationService;
import ru.moore.web.service.SignService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/roleAdmin")
public class AdminFilter implements Filter {

    private AuthenticationService authentication;;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        authentication = springContext.getBean(AuthenticationService.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        if (isProtected(req.getRequestURI())) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } else {
            resp.setStatus(404);
            resp.sendRedirect("/userChat");
        }
    }

    private boolean isProtected(String path) {
        if (path.startsWith("/roleAdmin") && authentication.getAuthUser().getRole().equals("ADMIN")) {
            return true;
        }
        return false;
    }

    @Override
    public void destroy() {

    }

}
