package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ru.moore.web.models.FilesUser;
import ru.moore.web.repositories.FilesUserRepository;
import ru.moore.web.service.ChatService;
import ru.moore.web.service.FileService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Component
public class FileServiceImpl implements FileService {

    @Value("${pathToFilesUser}")
    String path;
    @Autowired
    private ChatService chatService;
    @Autowired
    private FilesUserRepository filesUserRepository;
    @Autowired
    private FilesUserRepository fileRepository;

    @Override
    public void saveFile(InputStream inputStream, String fileType, String uuidUser) {
        try {
            String fileName = chatService.generateUuid();
            Files.copy(inputStream, Paths.get(path + fileName));
            inputStream.close();

            FilesUser file = FilesUser.builder()
                    .fileName(fileName)
                    .uuidUser(uuidUser)
                    .fileType(fileType)
                    .build();
            filesUserRepository.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<FilesUser> getAllFiles(String uuidUser) {
        return fileRepository.findByFile(uuidUser);
    }

    @Override
    public Optional<FilesUser> getFile(String uuidFile) {
        return fileRepository.find(uuidFile);
    }

    @Override
    public String getPath() {
        return path;
    }
}
