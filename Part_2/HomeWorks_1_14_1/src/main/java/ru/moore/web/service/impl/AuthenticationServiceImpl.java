package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.web.models.User;
import ru.moore.web.service.AuthenticationService;
import ru.moore.web.repositories.UsersRepository;

import java.util.Optional;
import java.util.UUID;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {

    public User user;
    private Boolean isAuth;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean authenticated(String login, String password) {
        Optional<User> userOptional = usersRepository.findByLogin(login, password);
        if (userOptional.isPresent()) {
            userOptional.get().setUuid(UUID.randomUUID().toString());
            usersRepository.update(userOptional.get());
            user=userOptional.get();
            isAuth = true;
            return isAuth;
        }
        return false;
    }

    public User getAuthUser(){
        return user;
    }

    public Boolean isAuth(){
        return isAuth;
    }

}
