package ru.moore.web.service;

import ru.moore.web.models.FilesUser;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public interface FileService {

    void saveFile(InputStream inputStream, String fileType, String uuidUser);

    List<FilesUser> getAllFiles(String uuidUser);

    Optional<FilesUser> getFile(String uuidFile);

    String getPath ();
}
