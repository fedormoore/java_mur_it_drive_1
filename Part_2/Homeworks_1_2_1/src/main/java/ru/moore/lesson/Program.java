package ru.moore.lesson;

import com.beust.jcommander.JCommander;
import ru.moore.FileInfo;

public class Program {
    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        FileInfo fileInfo = new FileInfo();

        //System.out.println(arguments.filePatch);
        String result[][] = fileInfo.print(arguments.filePatch);
        for (int i = 0; i < result.length; i++) {
            System.out.println("Name file - " + result[i][0] + " size - " + result[i][1]);
        }
    }
}
