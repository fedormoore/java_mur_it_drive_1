<%@ page import="java.util.List" %>
<%@ page import="ru.moore.web.models.User" %>
<%@ page import="ru.moore.web.models.Room" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <th>Логин</th>
        <th>Удалить</th>
        <th>Добавить в комнату</th>
    </tr>
    <%
        List<User> users = (List<User>) request.getAttribute("users");
        List<Room> rooms = (List<Room>) request.getAttribute("rooms");
        for (User user : users) {
    %>
    <tr>
        <td>
            <%=user.getLogin()%>
        </td>
        <td>
            <form action="/usersList?action=delete&userId=<%=user.getId()%>" method="post">
                <input type="hidden" value="${_csrf_token}" name="_csrf_token">
                <input type="submit" value="Удалить">
            </form>
        </td>
        <td>
            <form action="/room?action=addUserInRoom&userId=<%=user.getId()%>" method="post">
                <input type="hidden" value="${_csrf_token}" name="_csrf_token">
                <select name="roomId">
                    <%
                        for (Room room : rooms) {
                    %>
                    <option value="<%=room.getId()%>"><%=room.getNameRoom()%></option>
                    <%
                        }
                    %>
                </select>
                <input type="submit" value="Добавить">
            </form>

        </td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
