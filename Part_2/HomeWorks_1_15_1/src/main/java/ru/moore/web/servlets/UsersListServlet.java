package ru.moore.web.servlets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.service.FileService;
import ru.moore.web.service.RoomService;
import ru.moore.web.service.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/usersList")
public class UsersListServlet extends HttpServlet {

    private UsersService usersService;
    private RoomService roomService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        usersService = springContext.getBean(UsersService.class);
        roomService = springContext.getBean(RoomService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = usersService.findAll();
        if (users.size() > 0) {
            List<Room> rooms = roomService.findAll();
            req.setAttribute("users", users);
            req.setAttribute("rooms", rooms);
            req.getRequestDispatcher("jsp/usersList.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            usersService.deleteUserById(Long.parseLong(req.getParameter("userId")));
        }
        resp.sendRedirect("/usersList");
    }
}
