package ru.moore.web.servlets.files;

import org.springframework.context.ApplicationContext;
import ru.moore.web.models.FilesUser;
import ru.moore.web.service.FileService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/filesList")
public class FilesListServlet extends HttpServlet {

    private FileService fileService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        fileService = springContext.getBean(FileService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuidUser = null;
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("UUID")) {
                uuidUser = cookie.getValue();
            }
        }
        List<FilesUser> files = fileService.getAllFiles(uuidUser);
        if (files.size() > 0) {
            List<String> link = new ArrayList<>();
            for (FilesUser file : files) {
                link.add(file.getFileName());
            }
            req.setAttribute("link", link);
            req.getRequestDispatcher("jsp/filesList.jsp").forward(req, resp);
        }
    }
}
