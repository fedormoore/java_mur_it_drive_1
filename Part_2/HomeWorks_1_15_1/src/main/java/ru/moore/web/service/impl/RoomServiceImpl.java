package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.web.models.Room;
import ru.moore.web.repositories.RoomRepository;
import ru.moore.web.service.RoomService;

import java.util.List;

@Component
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public void createRoom(Room room) {
        roomRepository.save(room);
    }

    @Override
    public List<Room> findAll() {
        return roomRepository.findAll();
    }

    @Override
    public void saveUserInRoom(Long userId, Long roomId) {
        roomRepository.saveUserInRoom(userId, roomId);
    }
}
