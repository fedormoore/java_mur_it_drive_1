package ru.moore.web.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.models.Room;
import ru.moore.web.repositories.RoomRepository;
import ru.moore.web.repositories.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository {

    private DataSource dataSource;

    private final String SQL_FIND_BY_NAME = "select * from room where name_room = ?";
    private final String SQL_FIND_ALL = "select * from room";
    private static final String SQL_INSERT_ROOM = "insert into room (name_room, max_users) values (?, ?)";
    private static final String SQL_SAVE_USER_IN_ROOM = "insert into users_room (id_user, id_room) values (?, ?)";

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            return new Room(
                    row.getLong("id"),
                    row.getString("name_room"),
                    row.getInt("max_users")
            );
        }
    };

    @Override
    public Room findByNameRoom(String roomName) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NAME);
            statement.setString(1, roomName);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return roomRowMapper.mapRow(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
    }

    @Override
    public void saveUserInRoom(Long userId, Long roomId) {
        PreparedStatement statement = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SAVE_USER_IN_ROOM);
            statement.setLong(1, userId);
            statement.setLong(2, roomId);
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Room save not executed");
            }

            statement.getGeneratedKeys();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, null, connection);
        }
    }

    @Override
    public Room save(Room object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ROOM, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getNameRoom());
            statement.setInt(2, object.getMaxUsers());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Room save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys, connection);
        }
        return object;
    }

    @Override
    public void update(Room object) {

    }

    @Override
    public Optional<Room> find(String roomName) {
        return null;
    }

    @Override
    public List<Room> findAll() {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            List<Room> result = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Room room = roomRowMapper.mapRow(resultSet);
                result.add(room);
            }

            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
    }

    private void closedStatementResult(PreparedStatement statement, ResultSet resultSet, Connection connection) {
        try {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ignored) {
        }
    }

}
