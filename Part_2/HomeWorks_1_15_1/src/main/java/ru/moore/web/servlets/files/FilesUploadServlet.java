package ru.moore.web.servlets.files;

import org.springframework.context.ApplicationContext;
import ru.moore.web.service.FileService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FileService fileService;

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        fileService = springContext.getBean(FileService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("file");
        String fileType = part.getContentType();
        InputStream inputStream = part.getInputStream();

        String uuidUser = null;
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("UUID")) {
                uuidUser = cookie.getValue();
            }
        }

        fileService.saveFile(inputStream, fileType, uuidUser);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("html/userChat.html");
        requestDispatcher.forward(req, resp);
    }
}

