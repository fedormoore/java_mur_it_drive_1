package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.web.models.User;
import ru.moore.web.repositories.UsersRepository;
import ru.moore.web.service.ChatService;
import ru.moore.web.service.SignService;

import java.util.Optional;

@Component
public class SignServiceImpl implements SignService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ChatService chatService;

    @Override
    public Boolean signUp(String login, String password) {
        if (!usersRepository.findByLogin(login).isPresent()) {
            User user = User.builder()
                    .login(login)
                    .password(password)
                    .uuid(chatService.generateUuid())
                    .build();
            usersRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public Optional<User> signIn(String login, String password) {
        Optional<User> user = usersRepository.findByLoginAndPassword(login, password);
        if (user.isPresent()) {
            user.get().setUuid(chatService.generateUuid());
            usersRepository.update(user.get());
        }
        return user;
    }
}
