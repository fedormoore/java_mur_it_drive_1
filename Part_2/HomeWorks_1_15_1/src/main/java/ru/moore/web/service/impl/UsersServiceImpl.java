package ru.moore.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.web.models.User;
import ru.moore.web.repositories.UsersRepository;
import ru.moore.web.service.UsersService;

import java.util.List;
import java.util.Optional;

@Component
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;


    @Override
    public void deleteUserById(long id) {
        usersRepository.findById(id)
                .ifPresent(
                        user -> {
                            user.setIsDeleted(true);
                            usersRepository.update(user);
                        }
                );
    }

    @Override
    public List<User> findAll() {
        return usersRepository.findAll();
    }
}
