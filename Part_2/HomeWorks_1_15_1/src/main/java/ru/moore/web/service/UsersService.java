package ru.moore.web.service;

import ru.moore.web.models.User;

import java.util.List;

public interface UsersService {

    void deleteUserById(long id);

    List<User> findAll();

}
