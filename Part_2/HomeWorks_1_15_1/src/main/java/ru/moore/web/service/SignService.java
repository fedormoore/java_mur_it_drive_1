package ru.moore.web.service;

import ru.moore.web.models.User;

import java.util.Optional;

public interface SignService {

    Boolean signUp(String login, String password);

    Optional<User> signIn(String login, String password);
}
