package ru.moore.web.filters;

import ru.moore.web.models.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        HttpSession session = req.getSession(false);
        User user = null;
        if (session != null) {
            user = (User) session.getAttribute("user");
            if (isProtected(req.getRequestURI(), user)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        } else {
            resp.setStatus(404);
            resp.sendRedirect("/signIn");
        }
    }

    private boolean isProtected(String path, User user) {
        if (path.startsWith("/usersList") && user.getRole().equals("ADMIN")) {
            return true;
        }
        return false;
    }

    @Override
    public void destroy() {

    }

}
