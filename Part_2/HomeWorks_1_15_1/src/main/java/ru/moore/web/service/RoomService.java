package ru.moore.web.service;

import ru.moore.web.models.Room;

import java.util.List;

public interface RoomService {

    void createRoom(Room room);

    List<Room> findAll();

    void saveUserInRoom(Long userId, Long roomId);
}
