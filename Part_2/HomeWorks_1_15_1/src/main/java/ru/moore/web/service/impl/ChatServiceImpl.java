package ru.moore.web.service.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.service.ChatService;

import java.util.UUID;

@Component
public class ChatServiceImpl implements ChatService {

    @Override
    public String generateUuid() {
        return UUID.randomUUID().toString();
    }

}
