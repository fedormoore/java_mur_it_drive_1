package ru.moore.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.moore.web.models.Room;
import ru.moore.web.service.RoomService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/room")
public class RoomServlet extends HttpServlet {

    private RoomService roomService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        roomService = springContext.getBean(RoomService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/createRoom.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("action") != null && req.getParameter("action").equals("createRoom")) {
            String nameRoom = req.getParameter("nameRoom");
            String maxUsers = req.getParameter("maxUsers");
            Room room = Room.builder()
                    .nameRoom(nameRoom)
                    .maxUsers(Integer.valueOf(maxUsers))
                    .build();
            roomService.createRoom(room);
            resp.sendRedirect("/userChat");
        }
        if (req.getParameter("action") != null && req.getParameter("action").equals("addUserInRoom")) {
            Long userId = Long.parseLong(req.getParameter("userId"));
            Long roomId = Long.parseLong(req.getParameter("roomId"));
            roomService.saveUserInRoom(userId, roomId);
            resp.sendRedirect("/usersList");
        }
    }
}
