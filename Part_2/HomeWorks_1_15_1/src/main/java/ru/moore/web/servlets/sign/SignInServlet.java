package ru.moore.web.servlets.sign;

import org.springframework.context.ApplicationContext;
import ru.moore.web.models.User;
import ru.moore.web.service.SignService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private SignService signService;

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        signService = springContext.getBean(SignService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/signIn.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        Optional<User> user = signService.signIn(login, password);
        if (user.isPresent()) {
            Cookie cookie = new Cookie("UUID", user.get().getUuid());
            resp.addCookie(cookie);
            HttpSession session = req.getSession();
            session.setAttribute("authenticated", true);
            session.setAttribute("user", user.get());
            resp.sendRedirect("/userChat");
        } else {
            resp.setStatus(403);
            req.getRequestDispatcher("jsp/signIn.jsp").forward(req, resp);
        }
    }
}
