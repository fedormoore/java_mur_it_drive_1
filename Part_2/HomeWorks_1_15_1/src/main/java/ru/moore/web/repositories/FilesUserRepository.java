package ru.moore.web.repositories;

import ru.moore.web.models.FilesUser;

import java.util.List;

public interface FilesUserRepository extends CrudRepository<FilesUser> {

    List<FilesUser> findByFile(String uuidUser);

}
