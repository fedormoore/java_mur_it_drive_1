package ru.moore.web.filters;

import org.springframework.context.ApplicationContext;
import ru.moore.web.service.ChatService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.moore.web.filters.ResponseUtil.sendForbidden;

public class CsrfFilter implements Filter {

    private ChatService chatService;

    private List<String> csrfList;

    @Override
    public void init(FilterConfig filterConfig) {
        ServletContext servletContext = filterConfig.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("springContext");
        chatService = springContext.getBean(ChatService.class);
        csrfList = new ArrayList<>();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String requestCsrf = request.getParameter("_csrf_token");
            for (int i=0; i<csrfList.size();i++) {
                if (csrfList.get(i).equals(requestCsrf)) {
                    csrfList.remove(i);
                    filterChain.doFilter(servletRequest, servletResponse);
                    return;
                }
            }
            sendForbidden(request, response);
            return;
        }
        if (request.getMethod().equals("GET")) {
            String csrf = chatService.generateUuid();
            csrfList.add(csrf);
            request.setAttribute("_csrf_token", csrf);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}

