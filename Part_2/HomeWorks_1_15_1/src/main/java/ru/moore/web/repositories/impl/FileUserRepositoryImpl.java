package ru.moore.web.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.models.FilesUser;
import ru.moore.web.repositories.FilesUserRepository;
import ru.moore.web.repositories.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class FileUserRepositoryImpl implements FilesUserRepository {

    private DataSource dataSource;

    private final String SQL_INSERT_FILE = "insert into files (file_name, uuid_user, type_file) values (?, ?, ?)";
    private final String SQL_FIND_ALL_FILE = "select * from files where uuid_user = ?";
    private final String SQL_FIND_FILE = "select * from files where file_name = ?";

    public FileUserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<FilesUser> fileRowMapper = new RowMapper<FilesUser>() {
        public FilesUser mapRow(ResultSet row) throws SQLException {
            return new FilesUser(
                    row.getLong("id"),
                    row.getString("file_name"),
                    row.getString("uuid_user"),
                    row.getString("type_file")
            );
        }
    };

    @Override
    public List<FilesUser> findByFile(String uuidUser) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            List<FilesUser> result = new ArrayList<>();
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_FILE);
            statement.setString(1, uuidUser);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                FilesUser room = fileRowMapper.mapRow(resultSet);
                result.add(room);
            }

            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
    }

    @Override
    public FilesUser save(FilesUser object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_FILE, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getFileName());
            statement.setString(2, object.getUuidUser());
            statement.setString(3, object.getFileType());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("File save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys, connection);
        }
        return object;
    }

    @Override
    public void update(FilesUser object) {

    }

    @Override
    public Optional<FilesUser> find(String uuid) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_FILE);
            statement.setString(1, uuid);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.of(fileRowMapper.mapRow(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet, connection);
        }
    }

    @Override
    public List<FilesUser> findAll() {
        return null;
    }

    private void closedStatementResult(PreparedStatement statement, ResultSet resultSet, Connection connection) {
        try {
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ignored) {
        }
    }
}
