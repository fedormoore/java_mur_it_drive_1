package ru.moore.lesson;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {

    @Parameter(names = {"--filePatch"})
    public String filePatch;

}
