package ru.moore.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Server {

    private Socket socketClient;

    private LinkedList<ThreadClient> userList = new LinkedList<>();


    public void start(int port) {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
            while (true) {
                socketClient = serverSocket.accept();
                ThreadClient threadClient = new ThreadClient(socketClient);
                threadClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class ThreadClient extends Thread {

        private Socket socketClient;
        private PrintWriter writer;
        private BufferedReader clientReader;
        private String inputLine;

        public ThreadClient(Socket socketClient) {
            this.socketClient = socketClient;
            try {
                synchronized (userList) {
                    userList.add(this);
                }
                InputStream clientInputStream = socketClient.getInputStream();
                clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    inputLine = clientReader.readLine();
                    while (inputLine != null) {
                        synchronized (userList) {
                            for (ThreadClient co : userList) {
                                writer = new PrintWriter(co.socketClient.getOutputStream(), true);
                                writer.println(inputLine);
                            }
                        }
                        inputLine = clientReader.readLine();
                    }
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
