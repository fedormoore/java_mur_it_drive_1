package ru.moore.server;

import com.beust.jcommander.JCommander;

public class MainServer {

    public static void main(String[] args) {

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        Server server = new Server();
        //server.start(Integer.valueOf(arguments.port));
        server.start(7777);
    }
}
