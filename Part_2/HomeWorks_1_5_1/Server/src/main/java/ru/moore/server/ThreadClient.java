package ru.moore.server;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;

public class ThreadClient extends Thread {

    static LinkedList<ThreadClient> userList = new LinkedList<>();

    private Socket socketClient;
    PrintWriter writer;
    BufferedReader clientReader;
    String inputLine;

    public ThreadClient(Socket socketClient) {
        this.socketClient = socketClient;
        try {
            synchronized (userList) {
                userList.add(this);
            }
            InputStream clientInputStream = socketClient.getInputStream();
            clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                inputLine = clientReader.readLine();
                while (inputLine != null) {
                    System.out.println(inputLine);

                    synchronized (userList) {
                        for (ThreadClient co : userList) {
                            writer = new PrintWriter(co.socketClient.getOutputStream(), true);
                            writer.println(inputLine);
                        }
                    }

                    inputLine = clientReader.readLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
