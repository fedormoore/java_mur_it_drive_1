package ru.moore.client;

import com.beust.jcommander.IStringConverter;

import java.util.ArrayList;
import java.util.List;

public class ArgumentsListConverter implements IStringConverter<List<String>> {

    @Override
    public List<String> convert(String serverParameters) {
        String[] paths = serverParameters.split(",");
        List<String> list = new ArrayList<String>();
        for (String path : paths) {
            list.add(path);
        }
        return list;
    }
}
