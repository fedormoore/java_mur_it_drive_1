package ru.moore.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {

    private Socket client;
    private PrintWriter toServer;
    private BufferedReader fromServer;

    public Client(String host, int port) {
        try {
            client = new Socket(host, port);
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessagesTask).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(String message){
        toServer.println(message);
    }

    private Runnable receiverMessagesTask = new Runnable() {
        public void run() {
            while (true) {
                try {
                    String messageFromServer = fromServer.readLine();
                    if (messageFromServer != null) {
                        System.out.println(messageFromServer);
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    };
}
