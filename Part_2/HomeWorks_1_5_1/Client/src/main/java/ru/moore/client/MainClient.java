package ru.moore.client;

import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class MainClient {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        Client client = new Client(arguments.serverParameters.get(0), Integer.valueOf(arguments.serverParameters.get(1)));

        while (true) {
            String massega = scanner.nextLine();
            client.sendMessage(massega);
        }
    }
}
