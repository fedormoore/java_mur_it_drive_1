package ru.moore;

import org.springframework.beans.factory.annotation.Autowired;
import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.MessageRepository;
import ru.moore.repositories.RoomRepository;
import ru.moore.repositories.UsersRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

@Component
public class MessagesServiceImpl implements MessagesService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private MessageRepository messageRepository;

    private User user;
    private Room room;

    @Override
    public void signUp(String login) {
        user = User.builder()
                .name(login)
                .build();
        Optional<User> optionalUser = usersRepository.find(login);
        user = optionalUser.isPresent() ? optionalUser.get() : usersRepository.save(user);

        user.setRooms(roomRepository.findAllRoomByUserId(user));

        Optional<Room> optionalActiveRoom = roomRepository.findActiveRoom(user);
        user.setActiveRoom(optionalActiveRoom.isPresent() ? optionalActiveRoom.get() : null);
    }

    @Override
    public void chooseRoom(String roomName) {
        room = Room.builder()
                .nameRoom(roomName)
                .build();
        Optional<Room> optionalRoom = roomRepository.find(roomName);
        room = optionalRoom.isPresent() ? optionalRoom.get() : roomRepository.save(room);

        //Проверяем состоит ли пользователь в комнате
        boolean isRoom = false;
        for (Room room : user.getRooms()) {
            if (room.getNameRoom().equals(roomName)) {
                isRoom = true;
                break;
            }
        }

        if (isRoom) {
            user.setActiveRoom(room);
        } else {
            user.setActiveRoom(room);

            List<Room> rooms = new ArrayList<>();
            if (user.getRooms() != null) {
                rooms = user.getRooms();
            }
            rooms.add(new Room(room.getId(), room.getNameRoom()));

            roomRepository.saveUserRoom(user, room);
        }
    }

    @Override
    public void sendMessage(User user, Room room, String message) {
        //messageRepository.sendMessage(user, room, message);
    }

}
