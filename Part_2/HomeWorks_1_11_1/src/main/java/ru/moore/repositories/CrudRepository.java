package ru.moore.repositories;

import ru.moore.models.User;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {

    T save(T object);

    void update(T object);

    Optional<T> find(String name);

    List<T> findAll();

}
