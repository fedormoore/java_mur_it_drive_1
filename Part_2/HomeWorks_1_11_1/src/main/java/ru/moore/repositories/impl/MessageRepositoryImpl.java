package ru.moore.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.models.Message;
import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.MessageRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Component
public class MessageRepositoryImpl implements MessageRepository {

    private DataSource dataSource;

    private static final String SQL_GET_MESSAGE = "select * from message where id_room = ? order by id desc limit 30";
    private static final String SQL_INSERT_MESSAGE = "insert into message (id_users, id_room, message) values (?, ?, ?)";

    public MessageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void sendMessage(User user, Room room, String message) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MESSAGE);
            preparedStatement.setLong(1, user.getId());
            preparedStatement.setLong(2, room.getId());
            preparedStatement.setString(3, message);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Message save(Message object) {

        return null;
    }

    @Override
    public void update(Message object) {

    }

    @Override
    public Optional<Message> find(String name) {
        return Optional.empty();
    }

    @Override
    public List<Message> findAll() {
        return null;
    }
}
