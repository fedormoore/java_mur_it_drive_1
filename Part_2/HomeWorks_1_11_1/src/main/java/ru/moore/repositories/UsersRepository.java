package ru.moore.repositories;

import ru.moore.models.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User> {

}
