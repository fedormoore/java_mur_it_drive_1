package ru.moore.repositories;

import ru.moore.models.Room;
import ru.moore.models.User;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends CrudRepository<Room> {

    //Ищем комнаты которые принадлежат пользователю
    List<Room> findAllRoomByUserId(User user);

    //Ищем активную комнату пользователя
    Optional<Room> findActiveRoom(User user);

    //Сохраняем пользователя в комнате
    void saveUserRoom(User user, Room room);


}
