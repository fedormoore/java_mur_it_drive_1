package ru.moore.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.models.Room;
import ru.moore.models.User;
import ru.moore.repositories.RoomRepository;
import ru.moore.repositories.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository {

    private DataSource dataSource;

    //Ищем комнату по ее названию
    private final String SQL_SELECT_ROOM = "select * from room where nameroom = ?";
    private static final String SQL_INSERT_ROOM = "insert into room (nameroom) values (?)";

    //Сохраняем пользователя в комнате и делаем ее активной
    private static final String SQL_SET_ACTIVE_ROOM = "insert into users_room (id_user, id_room, active) values (?, ?, ?)";
    //Убираем активную комнату
    private static final String SQL_RESET_ACTIVE_ROOM = "update users_room set active = null where id_user = ?";

    //Ищем комнаты которые принадлежат пользователю
    private final String SQL_SELECT_ALL_ROOM_USER = "select r.id, r.nameroom from users_room as ur\n" +
            "left join room as r on r.id = ur.id_room\n" +
            "where ur.id_user = ?";

    //Ищем активную комнату пользователя
    private final String SQL_SELECT_ACTIVE_ROOM = "select r.id, r.nameroom from users_room as ur\n" +
            "left join room as r on r.id = ur.id_room\n" +
            "where ur.id_user = ? and ur.active is not null";

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            return new Room(
                    row.getLong("id"),
                    row.getString("nameRoom")
            );
        }
    };

    //Ищем комнаты которые принадлежат пользователю
    @Override
    public List<Room> findAllRoomByUserId(User user) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            List<Room> result = new ArrayList<>();
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ALL_ROOM_USER);
            statement.setLong(1, user.getId());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Room room = roomRowMapper.mapRow(resultSet);
                result.add(room);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet);
        }
    }

    //Ищем активную комнату пользователя
    @Override
    public Optional<Room> findActiveRoom(User user) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ACTIVE_ROOM);
            statement.setLong(1, user.getId());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(roomRowMapper.mapRow(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet);
        }
    }

    //Сохраняем пользователя в комнате
    @Override
    public void saveUserRoom(User user, Room room) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_RESET_ACTIVE_ROOM);
            statement.setLong(1, user.getId());
            statement.executeUpdate();

            statement = connection.prepareStatement(SQL_SET_ACTIVE_ROOM);
            statement.setLong(1, user.getId());
            statement.setLong(2, room.getId());
            statement.setLong(3, room.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Room save not executed");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, null);
        }
    }

    //Сохраняем комнату
    @Override
    public Room save(Room object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ROOM, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getNameRoom());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Room save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys);
        }
        return object;
    }

    //Обновляем комнату
    @Override
    public void update(Room object) {

    }

    //Ищем комнату по ее названию
    @Override
    public Optional<Room> find(String roomName) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ROOM);
            statement.setString(1, roomName);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(roomRowMapper.mapRow(resultSet));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet);
        }
    }

    @Override
    public List<Room> findAll() {
        return null;
    }

    public void closedStatementResult(PreparedStatement statement, ResultSet resultSet) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignored) {
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ignored) {
            }
        }
    }

}
