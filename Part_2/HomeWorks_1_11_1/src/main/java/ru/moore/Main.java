package ru.moore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.moore.config.ApplicationConfig;

public class Main {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        SocketServer server = context.getBean(SocketServer.class);
        server.start(7575);
    }

}
