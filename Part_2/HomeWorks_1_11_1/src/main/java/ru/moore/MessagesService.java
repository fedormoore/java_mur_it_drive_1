package ru.moore;

import ru.moore.models.Room;
import ru.moore.models.User;

public interface MessagesService {

    void signUp(String login);

    void chooseRoom(String roomName);

    void sendMessage(User user, Room room, String message);

}
