package ru.moore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moore.models.Room;
import ru.moore.models.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SocketServer {

    private static final String SIGN_UP_COMMAND = "signUp";
    private static final String CHOOSE_ROOM_COMMAND = "choose room";
    private static final String EXIT_ROOM_COMMAND = "exit from room";
    private static final String SEND_MESSAGE_COMMAND = "send message";

    @Autowired
    private MessagesService messagesService;

    private List<ChatSocketClient> anonyms = new ArrayList<>();
    private Map<Long, List<ChatSocketClient>> users = new HashMap<>();

    public SocketServer(MessagesService messagesService) {
        this.messagesService = messagesService;
    }

    public void start(int port) {
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);

            while (true) {
                Socket socketClient = serverSocket.accept();
                ChatSocketClient threadClient = new ChatSocketClient(socketClient);
                threadClient.start();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class ChatSocketClient extends Thread {

        private Socket socket;
        private BufferedReader input;
        private PrintWriter output;

        private String inputLine;

        private Long userId;
        private Long roomId;

        public ChatSocketClient(Socket socket) {
            this.socket = socket;

            try {
                synchronized (anonyms) {
                    anonyms.add(this);
                }
                InputStream clientInputStream = socket.getInputStream();
                input = new BufferedReader(new InputStreamReader(clientInputStream));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    inputLine = input.readLine();
                    synchronized (users) {
                        //SIGN_UP_COMMAND
                        if (isSignUpCommand(inputLine)) {
                            String information = exctractInformation(SIGN_UP_COMMAND, inputLine);

                            messagesService.signUp(information);

                            anonyms.remove(this);
                            List<ChatSocketClient> lt = new ArrayList<>();
                            lt.add(this);
                            users.put(roomId, lt);

                                /*userId = user.getId();
                                if (user.getActiveRoom()!=null)
                                roomId = user.getActiveRoom().getId();*/
                        }

                        //CHOOSE_ROOM_COMMAND
                        if (isChooseRoomCommand(inputLine)) {
                            String information = exctractInformation(CHOOSE_ROOM_COMMAND, inputLine);
                            //room.setNameRoom(information);
                            messagesService.chooseRoom(information);
                            //roomId = user.getActiveRoom().getId();
                        }
                        /*if (isExitRoomCommand(inputLine)) {

                        }
                        if (isSendMessageCommand(inputLine)) {
                            for (int i = 0; i < users.get(user.getActiveRoom().getId()).size(); i++) {
                                Long activeRoomIdOfUser = user.getActiveRoom().getId();
                                String information = exctractInformation(SEND_MESSAGE_COMMAND, inputLine);

                                output = new PrintWriter(users.get(activeRoomIdOfUser).get(i).socket.getOutputStream(), true);
                                output.println(user.getName() + " -> " + information);
                                messagesService.sendMessage(user, room, information);
                            }
                        }*/
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }

        public boolean isSignUpCommand(String inputLine) {
            return inputLine.startsWith(SIGN_UP_COMMAND);
        }

        public boolean isChooseRoomCommand(String inputLine) {
            return inputLine.startsWith(CHOOSE_ROOM_COMMAND);
        }

        public boolean isExitRoomCommand(String inputLine) {
            return inputLine.startsWith(EXIT_ROOM_COMMAND);
        }

        public boolean isSendMessageCommand(String inputLine) {
            return inputLine.startsWith(SEND_MESSAGE_COMMAND);
        }

    }

    private String exctractInformation(String command, String inputLine) {
        return inputLine.substring(command.length(), inputLine.length());
    }
}
