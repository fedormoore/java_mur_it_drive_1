package ru.moore;

import java.io.File;

public class FileInfo {

    public String[][] print(String folder) {
        File folderEntries = new File(folder);
        String listFiles[][] = new String[folderEntries.listFiles().length][2];
        for (int i = 0; i < folderEntries.listFiles().length; i++) {
            listFiles[i][0]=folderEntries.listFiles()[i].getName();
            listFiles[i][1]=String.valueOf(folderEntries.listFiles()[i].length());
        }
        return listFiles;
    }

}