package ru.moore.models;

public class Course {
    private Integer id;
    private String title;
    private String start_date;
    private String finish_date;
    private String name;

    public Course(Integer id, String title, String start_date, String finish_date, String name) {
        this.id = id;
        this.title = title;
        this.start_date = start_date;
        this.finish_date = finish_date;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(String finish_date) {
        this.finish_date = finish_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", start_date='" + start_date + '\'' +
                ", finish_date='" + finish_date + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
