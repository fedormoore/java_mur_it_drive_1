package ru.moore;

import ru.moore.models.Course;
import ru.moore.models.Lesson;
import ru.moore.repositoies.CourseRepository;
import ru.moore.repositoies.CourseRepositoryImpl;
import ru.moore.repositoies.LessonRepository;
import ru.moore.repositoies.LessonRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class Main {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty555";


    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        CourseRepository courseRepository = new CourseRepositoryImpl(connection);
        Course course = courseRepository.find(1);
        System.out.println(course);
        System.out.println("---");
        List<Course> courseList = courseRepository.findAll();
        System.out.println(courseList);

        System.out.println("------------------------------------------");

        LessonRepository lessonRepository = new LessonRepositoryImpl(connection);
        Lesson lesson = lessonRepository.find(1);
        System.out.println(lesson);
        System.out.println("---");
        List<Lesson> lessonList = lessonRepository.findAll();
        System.out.println(lessonList);
    }
}
