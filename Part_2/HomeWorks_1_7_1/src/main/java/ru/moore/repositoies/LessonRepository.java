package ru.moore.repositoies;

import ru.moore.models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson> {
}
