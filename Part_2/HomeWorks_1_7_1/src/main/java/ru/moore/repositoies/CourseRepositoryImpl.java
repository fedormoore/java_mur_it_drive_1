package ru.moore.repositoies;

import ru.moore.models.Course;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CourseRepositoryImpl implements CourseRepository {

    private Connection connection;

    private static final String SQL_SELECT_ALL = "select * from course join lesson l on course.id = l.course_id";
    private static final String SQL_SELECT_BY_ID = "select * from course join lesson l on course.id = l.course_id where course.id = ";

    public CourseRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet row) throws SQLException{
            return new Course(
                    row.getInt("id"),
                    row.getString("title"),
                    row.getString("start_date"),
                    row.getString("finish_date"),
                    row.getString("name")
            );
        }
    };

    public void save(Course object) {

    }

    public void update(Course object) {

    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            return courseRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Course> findAll() {
        try {
            List<Course> result = new ArrayList<Course>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Course course = courseRowMapper.mapRow(resultSet);
                result.add(course);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
