package ru.moore.repositoies;

import ru.moore.models.Course;
import ru.moore.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LessonRepositoryImpl implements LessonRepository {

    private Connection connection;

    private static final String SQL_SELECT_ALL = "select * from lesson join course c on lesson.course_id = c.id";
    private static final String SQL_SELECT_BY_ID = "select * from lesson join course c on lesson.course_id = c.id where lesson.id = ";

    private RowMapper<Lesson> courseRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("title"),
                    row.getString("start_date"),
                    row.getString("finish_date")
            );
        }
    };

    public LessonRepositoryImpl(Connection connection) {
        this.connection=connection;
    }

    public void save(Lesson object) {

    }

    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            return courseRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<Lesson> findAll() {
        try {
            List<Lesson> result = new ArrayList<Lesson>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while (resultSet.next()) {
                Lesson lesson = courseRowMapper.mapRow(resultSet);
                result.add(lesson);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
