package ru.moore.web;

import org.springframework.beans.factory.annotation.Autowired;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.repositories.RoomRepository;
import ru.moore.web.repositories.UsersRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ChatServiceImpl implements ChatService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RoomRepository roomRepository;

    @Override
    public void signUp(String login) {
        User user = User.builder()
                .name(login)
                .build();
        Optional.ofNullable(usersRepository.findByLogin(login)).orElse(usersRepository.save(user));
    }

    @Override
    public void createRoom(String roomName, Integer maxUsers) {
        Room room = Room.builder()
                .nameRoom(roomName)
                .maxUsers(maxUsers)
                .build();
        Optional.ofNullable(roomRepository.findByNameRoom(roomName)).orElse(roomRepository.save(room));
    }
}
