package ru.moore.web.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.repositories.RowMapper;
import ru.moore.web.repositories.UsersRepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryImpl implements UsersRepository {

    private DataSource dataSource;

    //Добавляет нового пользователя в базу
    private final String SQL_INSERT_USER = "insert into users (login) values (?)";
    //Ищем пользователя по логину в базе
    private final String SQL_SELECT_USER = "select * from users where login = ?";

    public UserRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            List<Room> rooms = new ArrayList<>();
            return new User(
                    row.getLong("id"),
                    row.getString("login"),
                    rooms,
                    new Room()
            );
        }
    };

    @Override
    public User findByLogin(String login) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_USER);
            statement.setString(1, login);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return userRowMapper.mapRow(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet);
        }
    }

    //Добавляет нового пользователя в базу
    @Override
    public User save(User object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getName());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("User save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys);
        }
        return object;
    }

    //Обновляем информацию о пользователе
    @Override
    public void update(User object) {

    }

    //Ищем пользователя по логину в базе
    @Override
    public User find(String login) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    public void closedStatementResult(PreparedStatement statement, ResultSet resultSet) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignored) {
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ignored) {
            }
        }
    }
}
