package ru.moore.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room {

    private Long id;
    private String nameRoom;
    private int maxUsers;
    //private List<User> users;
}
