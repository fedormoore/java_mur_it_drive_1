package ru.moore.web.repositories;

import ru.moore.web.models.User;

public interface UsersRepository extends CrudRepository<User> {

    User findByLogin(String login);

}
