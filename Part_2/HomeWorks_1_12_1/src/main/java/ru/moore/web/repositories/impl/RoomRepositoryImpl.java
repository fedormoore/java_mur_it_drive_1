package ru.moore.web.repositories.impl;

import org.springframework.stereotype.Component;
import ru.moore.web.models.Room;
import ru.moore.web.models.User;
import ru.moore.web.repositories.RoomRepository;
import ru.moore.web.repositories.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RoomRepositoryImpl implements RoomRepository {

    private DataSource dataSource;

    //Ищем комнату по ее названию
    private final String SQL_SELECT_ROOM = "select * from room where name_room = ?";
    private static final String SQL_INSERT_ROOM = "insert into room (name_room, max_users) values (?, ?)";

    public RoomRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            return new Room(
                    row.getLong("id"),
                    row.getString("name_room"),
                    row.getInt("max_users")
            );
        }
    };

    @Override
    public Room findByNameRoom(String roomName) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_ROOM);
            statement.setString(1,roomName);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return roomRowMapper.mapRow(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            closedStatementResult(statement, resultSet);
        }
    }

    @Override
    public Room save(Room object) {
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ROOM, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, object.getNameRoom());
            statement.setInt(2, object.getMaxUsers());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Room save not executed");
            }

            generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                Long id = generatedKeys.getLong("id");
                object.setId(id);
            } else {
                throw new SQLException("Something wrong");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closedStatementResult(statement, generatedKeys);
        }
        return object;
    }

    @Override
    public void update(Room object) {

    }

    @Override
    public Room find(String roomName) {
        return null;
    }

    @Override
    public List<Room> findAll() {
        return null;
    }

    public void closedStatementResult(PreparedStatement statement, ResultSet resultSet) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignored) {
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ignored) {
            }
        }
    }

}
