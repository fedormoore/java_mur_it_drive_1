package ru.moore.web;

import ru.moore.web.models.Room;
import ru.moore.web.models.User;

public interface ChatService {

    void signUp(String login);

    void createRoom(String roomName, Integer maxUsers);
}
